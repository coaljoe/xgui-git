package xgui

import (
//"github.com/veandco/go-sdl2/gfx"
//"github.com/veandco/go-sdl2/sdl"
)

//"github.com/veandco/go-sdl2/sdl"

/*
func drawLoopY(tex *sdl.Texture, x, y, h int) {
	th := tex.
	for i := 0; i <
}
*/

// XXX copy-pasted from main
func drawRectangleColorWidth(x, y, w, h int, color Color, lineWidth int) {
	//lineWidth := int32(2)
	//lineWidth := int32(1)
	//lineWidth = 10
	//lineColor := sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
	//lineColor := sdl.Color{255, 255, 255, 64}
	//lineColor := sdl.Color{255, 255, 255, 255}
	topLeftPointX := x
	topLeftPointY := y
	topRightPointX := x + w
	topRightPointY := y
	bottomLeftPointX := x
	bottomLeftPointY := y + h
	bottomRightPointX := x + w
	bottomRightPointY := y + h
	_ = bottomRightPointX
	_ = bottomRightPointY

	// Draw top line
	/*
		if !gfx.ThickLineColor(renderer, int32(topLeftPointX), int32(topLeftPointY),
			int32(topRightPointX), int32(topRightPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	drawHLineWidth(topLeftPointX, topLeftPointY, w, color, lineWidth)
	// Draw bottom line
	/*
		if !gfx.ThickLineColor(renderer, int32(bottomLeftPointX), int32(bottomLeftPointY),
			int32(bottomRightPointX), int32(bottomRightPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	drawHLineWidth(bottomLeftPointX, bottomLeftPointY, w, color, lineWidth)
	// Draw left line
	/*
		if !gfx.ThickLineColor(renderer, int32(topLeftPointX), int32(topLeftPointY),
			int32(bottomLeftPointX), int32(bottomLeftPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	drawVLineWidth(topLeftPointX, topLeftPointY, h, color, lineWidth)
	// Draw right line
	/*
		if !gfx.ThickLineColor(renderer, int32(topRightPointX), int32(topRightPointY),
			int32(bottomRightPointX), int32(bottomRightPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	drawVLineWidth(topRightPointX, topRightPointY, h, color, lineWidth)

	var _ = `

	// Debug
	if false {
		drawCross(topLeftPointX, topLeftPointY, 20, ColorRed)
		drawCross(topRightPointX, topRightPointY, 20, ColorRed)
	}
	`

}

var _ = `
func _drawFillRect(renderer *sdl.Renderer, x, y, w, h int, c sdl.Color) {
	/*
		var q sdl.BlendMode
		p(renderer.GetDrawBlendMode(&q))
		p(q)
		pp(c)
	*/
	err := renderer.SetDrawBlendMode(sdl.BLENDMODE_BLEND)
	if err != nil {
		panic(err)
	}

	renderer.SetDrawColor(uint8(c.R), uint8(c.G), uint8(c.B), uint8(c.A))
	var rect sdl.Rect
	rect.X = int32(x)
	rect.Y = int32(y)
	rect.W = int32(w)
	rect.H = int32(h)
	renderer.FillRect(&rect)

	err = renderer.SetDrawBlendMode(sdl.BLENDMODE_NONE)
	if err != nil {
		panic(err)
	}
}
`

func drawFillRect(x, y, w, h int, c Color) {
	/*
		var q sdl.BlendMode
		p(renderer.GetDrawBlendMode(&q))
		p(q)
		pp(c)
	*/

	r := ctx.Renderer
	//xc := Color{int(c.R), int(c.G), int(c.B), int(c.A)}
	r.DrawRect(x, y, w, h, c)
}

var _ = `
func _drawLine(r *sdl.Renderer, x1, y1, x2, y2 int, c sdl.Color) {
	r.SetDrawColor(uint8(c.R), uint8(c.G), uint8(c.B), uint8(c.A))
	r.DrawLine(int32(x1), int32(y1), int32(x2), int32(y2))
}
`

func drawLine(x1, y1, x2, y2 int, c Color) {
	/*
		br := sb.NewSdlRenderer()
		br.Init(r)
		br.SetDrawColor(int(c.R), int(c.G), int(c.B), int(c.A))
		br.DrawLine(x1, y1, x2, y2)
	*/

	r := ctx.Renderer
	r.SetDrawColor(c.R(), c.G(), c.B(), c.A())
	r.DrawLine(x1, y1, x2, y2)
}

func drawHLine(x, y, l int, c Color) {
	drawLine(x, y, x+l, y, c)
}

func drawVLine(x, y, l int, c Color) {
	drawLine(x, y, x, y+l, c)
}

func drawHLineWidth(x, y, l int, c Color, width int) {
	shY := 0
	if width > 1 {
		shY = -(width / 2)
	}
	py := y
	for i := 0; i < width; i++ {
		drawHLine(x, py+shY, l, c)
		py++
	}
}

func drawVLineWidth(x, y, l int, c Color, width int) {
	shX := 0
	if width > 1 {
		shX = -(width / 2)
	}
	px := x
	for i := 0; i < width; i++ {
		drawVLine(px+shX, y, l, c)
		px++
	}
}

func drawCross(x, y, size int, c Color) {
	drawHLine(x-size/2, y, size, c)
	drawVLine(x, y-size/2, size, c)
}
