package xgui

import (
	"bitbucket.org/coaljoe/xgui/backend"
	. "bitbucket.org/coaljoe/xgui/types"
	//sb "bitbucket.org/coaljoe/xgui/backend/sdl"
	//"github.com/veandco/go-sdl2/sdl"
)

type TextAlignType int

const (
	TextAlignType_Left TextAlignType = iota
	TextAlignType_Center
	TextAlignType_Right
)

type TextVAlignType int

const (
	TextVAlignType_Top TextVAlignType = iota
	TextVAlignType_Middle
	TextVAlignType_Bottom
)

type LabelStyle struct {
	*Style
}

func NewLabelStyle() *LabelStyle {
	styleCopy := *ctx.Style
	s := &LabelStyle{
		Style: &styleCopy,
	}
	return s
}

// A widget.
type Label struct {
	*Widget
	Style *LabelStyle
	text  string
	//textColor Color
	//textFont   StyleOption
	//textColor  StyleOption
	TextAlign  TextAlignType
	TextVAlign TextVAlignType

	// Rendered text's texture
	image backend.ImageI

	prevText           string
	prevStyle *LabelStyle

	textW int
	textH int
	dirty bool

	//cb        func()
}

func (la *Label) Text() string { return la.text }

//func (la *Label) SetCallback(cb func()) { la.cb = cb }

//func NewLabel(rect Rect) *Label {
func NewLabel(pos Pos, text string) *Label {
	la := &Label{
		Widget: NewWidget("label"),
		//textColor: ctx.Style.textColor,
		//textColor: newStyleOption("textColor"),
		/*
			textFont:   newStyleOption(ctx.Style.textFont),
			textColor:  newStyleOption(&ctx.Style.textColor),
		*/
		TextAlign:  TextAlignType_Left,
		TextVAlign: TextVAlignType_Top,
	}
	la.Style = NewLabelStyle()
	la.BaseStyle = la.Style.Style
	//la.Style.OuterBorderColor = ColorGreen
	//la.prevStyle = *la.Style
	la.prevStyle = la.Style.Copy()

	la.rect[0] = pos[0]
	la.rect[1] = pos[1]
	//la.rect[2] = -1
	//la.rect[3] = -1
	la.rect[2] = 0
	la.rect[3] = 0
	la.dirty = true
	la.SetText(text)
	
	//Sub(Ev_mouse_enter, w.EvMouseEnter)
	//Sub(Ev_mouse_out, w.EvMouseOut)
	return la
}

func (la *Label) build() {
	/*
		ph := rxi.Renderer().GetPh()
		font := la.textFont.value().(*Font)
		la.rect[3] = float64(font.Height()) * ph
		//la.rect[3] = float64(32) * ph
	*/

	if la.text == "" {
		la.image = nil

		// Update rect size
		la.rect[2] = 0
		la.rect[3] = 0

		la.dirty = false
		return
	}

	// Update/fix widgets rect width and height
	tw, th := ctx.TextRenderer.SizeText(la.text, la.Style.TextFont)
	la.rect[2] = tw
	la.rect[3] = th
	la.textW = tw
	la.textH = th

	//p("XXX", tw, th)

	if la.image != nil {
		//la.image.(*sb.Image).Free()
		la.image.Free()
		la.image = nil
	}

	la.image = ctx.TextRenderer.RenderTextColorAsImage(la.text, tw, th,
		la.Style.TextColor, la.Style.TextFont)

	la.prevText = la.text

	la.dirty = false
}

func (la *Label) SetText(s string) {
	la.text = s
	la.build()
}

func (la *Label) SetPos(pos Pos) {
	la.rect[0] = pos[0]
	la.rect[1] = pos[1]
	la.build()
}

func (la *Label) Render() {
	//pp(6)
	r := ctx.Renderer
	_ = r

	if !la.visible {
		return
	}

	la.Widget.Render()
	defer la.Widget.RenderChildren()

	//px := la.rect.X()
	//py := la.rect.Y()

	//p("ZZZ")
	absRect := la.GetAbsoluteRect()
	px := absRect.X()
	py := absRect.Y()
	//p("ZZZ.")

	//tw, th := ctx.TextRenderer.SizeText(la.text, la.Style.TextFont)
	tw := la.textW
	th := la.textH

	if la.TextAlign == TextAlignType_Center {
		px -= tw / 2
		py -= th / 2
	} else if la.TextAlign == TextAlignType_Right {
		pp("not implemented")
	}

	if la.image != nil {
		//p(la.image.W(), la.image.H())
		//p(tw, th)
		//p(xtw, xth)
		//p("la.text:", la.text)
		//la.image.(*sb.Image).Surf().SaveBMP("image.bmp")
		//pp(2)
		la.image.Render(px, py, tw, th)
	}

	// XXX TODO
	//ctx.TextRenderer.RenderTextColor(la.text, px, py,
	//	la.Style.TextColor, la.Style.TextFont)
}

var _ = `
func (la *Label) Render(r *rx.Renderer) {
	//return
	la.Widget.Render(r)
	//sx := 2.0 / float64(r.Width())
	//sy := 2.0 / float64(r.Height())
	px := la.rect.X()
	py := la.rect.Y()
	//ctx.TextRenderer.RenderText(la.text, -1+8*sx, 1-50*sy, sx, sy)

	prevDepthTest := false
	if gl.IsEnabled(gl.DEPTH_TEST) {
		prevDepthTest = true
		gl.Disable(gl.DEPTH_TEST)
	}

	ctx.TextRenderer.RenderTextColor(la.text, px, py,
		*la.textColor.value().(*Color))
	//gl.Enable(gl.DEPTH_TEST)

	if prevDepthTest {
		gl.Enable(gl.DEPTH_TEST)
	}

	if false {
		//ctx.TextRenderer.RenderText("XXX", sx, sy, sx, sy)
		//ctx.TextRenderer.RenderText("YYY", 0, 0, sx, sy)
		//ctx.TextRenderer.RenderText("Последние новости", sx, sy)
		gl.Disable(gl.DEPTH_TEST)
		//ctx.TextRenderer.RenderText("Последние новости", 0.5, 0.5)
		ctx.TextRenderer.RenderText("Последние новости", .5, 0.5)
		ctx.TextRenderer.RenderTextColor("Последние новости 2", 0, 1, ColorGreen)
		gl.Enable(gl.DEPTH_TEST)
		gl.Disable(gl.DEPTH_TEST)
		//ctx.TextRenderer.RenderText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", -1+1*sx, 1-100*sy)
		gl.Enable(gl.DEPTH_TEST)
	}
}
`

func (la *Label) Update(dt float64) {
	la.Widget.Update(dt)

	/*
	if la.Style.TextColor != la.prevStyleTextColor {
		la.dirty = true
		la.prevStyleTextColor = la.Style.TextColor
	}
	*/
	//p("prev:", la.prevStyle.TextColor)
	//p("now:", la.Style.TextColor)
	//pf("%p %p\n", la.Style, la.prevStyle)
	//p(*la.Style == *la.prevStyle)

	if la.Style.TextColor != la.prevStyle.TextColor || 
		la.Style.TextFont != la.prevStyle.TextFont {
		la.dirty = true
		//p("dirty")
	}

	la.prevStyle = la.Style.Copy()

	// XXX move to update?
	if la.dirty {
		la.build()
	}
}
