package xgui

import (
	"bitbucket.org/coaljoe/lib/debug"
	"bitbucket.org/coaljoe/lib/xlog"
	"bitbucket.org/coaljoe/xgui/backend"
	//"fmt"
	//"github.com/veandco/go-sdl2/sdl"
	//sb "bitbucket.org/coaljoe/xgui/backend/sdl"
	//gb "bitbucket.org/coaljoe/xgui/backend/gl"
	//"flag"
	"github.com/veandco/go-sdl2/ttf"
	"os"
)

// Globals
//var ftctx *ft.Context
var xgi *XGui
var ctx *Context

var (
	_log *xlog.Logger
)

var _ = `
var optV = flag.Bool("v", false, "verbose output")

func init() {
	println("xgui.init()")
	//flag.Bool("v", false, "verbose output")
	flag.Parse()
}
`

func Init() {

	if debug.Verbose() {
		// TODO set verbose output for log too?
		p("in verbose mode")
	}

	_ = `
	
	verbose := false
	_ = verbose
	
	//flag.Bool("v", false, "verbose output")
	//flag.Parse()
	
	f := flag.Lookup("v")
	
	pv(f)
	pp(2)
	`

	// Create logger
	_log = xlog.NewLogger("xgui", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	if conf.DebugOutput {
		_log.SetOutputFile("xgui.log")
	}
	_log.SetWriteTime(true)
	_log.SetWriteFuncName(true)

	if !conf.DebugOutput {
		_log.SetEnabled(false)
	}

	if !conf.DebugOutput {
		debug.DisableP()
	}

	err := ttf.Init()
	if err != nil {
		panic(err)
	}
}

type XGui struct {
	//Context *Context
	*Context
	// Enable throttling
	throttling bool
	// Render only Nth frame
	throttlingNth uint64
	enabled       bool
	hasMouseFocus bool
}

func (xg *XGui) Enabled() bool             { return xg.enabled }
func (xg *XGui) SetEnabled(v bool)         { xg.enabled = v }
func (xg *XGui) Throttling() bool          { return xg.throttling }
func (xg *XGui) SetThrottling(v bool)      { xg.throttling = v }
func (xg *XGui) ThrottlingNth() uint64     { return xg.throttlingNth }
func (xg *XGui) SetThrottlingNth(v uint64) { xg.throttlingNth = v }

//func NewXGui(r *sdl.Renderer) *XGui {
func NewXGui(b backend.BackendI, conf_ *Conf) *XGui {
	if conf_ != nil {
		conf = conf_
	}

	Init() // fixme?
	xgi = &XGui{
		enabled:       true,
		throttling:    false,
		throttlingNth: 4,
	}

	xgi.Context = newContext()

	b.Register(xgi)

	r := b.Renderer()

	/*
		//r := b.Renderer().(*sb.SdlRenderer).Renderer()
		//r := nil
		var r *sdl.Renderer

		// Dummy renderer
		{
			var winTitle string = "Go-SDL2 Texture"
			var winWidth, winHeight int32 = 800, 600
			var window *sdl.Window
			var renderer *sdl.Renderer
			var err error

			window, err = sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
				winWidth, winHeight, sdl.WINDOW_SHOWN)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
				panic(1)
			}

			//renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
			surface, err2 := window.GetSurface()
			if err2 != nil {
				panic(2)
			}
			renderer, err = sdl.CreateSoftwareRenderer(surface)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
				panic(2)
			}

			r = renderer

		}
	*/

	/*
		pf("r: %#v\n", r)
		if r == nil {
			panic(2)
		}
	*/

	// Set Globals
	ctx = xgi.Context

	// Set renderer
	ctx.Renderer = r
	//ctx.Renderer = b.Renderer()

	ctx.Backend = b

	// Add Subsystems
	ctx.SheetSys = newSheetSys()
	ctx.InputSys = newInputSys()

	// Add extra context
	ctx.Style = NewStyle() // Default style
	//ctx.Font = loadFont("res/fonts/vera.ttf")
	ctx.TextRenderer = newTextRenderer()

	//rxi = rx.Rxi()
	//rxi.Renderer().AddGuiRP(xgi)

	//.Set vars
	SetDefaultVars()
	w, h := r.Size()
	vars.resX = w
	vars.resY = h

	return xgi
}

func (xg *XGui) Name() string {
	return "XGui"
}

func (xg *XGui) HasMouseFocus() bool {
	//for x := range xg.SheetSys.activeSheet.root {
	//}
	//return true
	return xg.hasMouseFocus
}

func (xg *XGui) InjectMouseButton(btn int, pressed bool) {
	p("XGui.InjectMouseButton")
	sdlBtn := uint8(btn)
	ctx.InputSys.OnMouseButton(sdlBtn, pressed)
}

func (xg *XGui) InjectMouseMove(x, y int) {
	p("XGui.InjectMouseMove")
	prevX := vars.mx
	prevY := vars.my

	vars.mx, vars.my = x, y
	sX := float64(x) / float64(vars.resX)
	sY := float64(y) / float64(vars.resY)
	evData := EvMouseMoveData{int(x), int(y), prevX, prevY, sX, sY,
		float64(prevX) / float64(vars.resX), float64(prevY) / float64(vars.resY)}
	p(evData)
	//xg.Pub(xg.Ev_gui_mouse_move, evData)
	ctx.InputSys.OnMouseMove(evData)
}

// Render the gui.
//func (xg *XGui) Render(r *sdl.Renderer) {
func (xg *XGui) Render() {
	pv("xg render:", xg)
	if !xg.enabled {
		return
	}

	//br := ctx.Backend.Renderer()
	r := ctx.Renderer

	r.PreRender()

	// XXX Fixme: first update gui?
	//xg.Update(rxi.App.GetDt())

	//println("XGui: Render")

	sh := ctx.SheetSys.ActiveSheet()
	if sh != nil {
		sh.Render()
	}

	r.PostRender()
}

func (xg *XGui) Update(dt float64) {
	p("xg update: %#v\n", xg)

	if !xg.enabled {
		return
	}
	//println("XGui: Update")

	sh := ctx.SheetSys.ActiveSheet()
	if sh != nil {
		//pdump(sh.Update)
		//pdump(sh)
		//fmt.Printf("-> %T\n", sh)
		//fmt.Printf("-> %v\n", sh)
		//fmt.Printf("-> %#v\n", sh)
		sh.Update(dt)
		//panic(2)
	}
}
