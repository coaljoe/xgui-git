package xgui

import (
	//"fmt"
	"github.com/veandco/go-sdl2/sdl"
	//ps "lib/pubsub"
	. "bitbucket.org/coaljoe/rx/math"
)

// Data for ev_mouse_move.
type EvMouseMoveData struct {
	X, Y           int
	PrevX, PrevY   int
	SX, SY         float64
	PrevSX, PrevSY float64
}

type InputSys struct {
	//Mouse    *rx.Mouse
	//Keyboard *rx.Keyboard
	//is       *rx.InputSys
	MX                      int
	MY                      int
	PrevMX                  int
	PrevMY                  int
	activeWidget            WidgetI
	prevActiveWidget        WidgetI
	lastPressedActiveWidget WidgetI
}

func newInputSys() *InputSys {
	//rxi := rx.Rxi()
	is := &InputSys{}
	//is:       rxi.InputSys,
	//Mouse:    rxi.InputSys.Mouse,
	//Keyboard: rxi.InputSys.Keyboard}
	is.init()
	return is
}

func (is *InputSys) init() {
	//Sub(rx.Ev_mouse_move, is.OnMouseMove)
	//Sub(rx.Ev_mouse_press, is.OnMousePress)
}

// Get active widget under the cursor
func (is *InputSys) getActiveWidget() WidgetI {

	mX := is.MX
	mY := is.MY
	prevMx := is.PrevMX
	prevMy := is.PrevMY

	// Now active widgets
	activeWidgets := make([]WidgetI, 0)
	// Prev active widgets
	prevActiveWidgets := make([]WidgetI, 0)
	//c.mx = p.X
	//c.my = p.Y
	sheet := ctx.SheetSys.ActiveSheet()
	
	// XXX fixme?
	if sheet == nil {
		return nil
	}
	
	for _, w := range sheet.GetWidgets() {
		p("w:", w.Name(), w)

		if !w.Visible() || !w.Active() {
			continue
		}

		if w.PassEvents() {
			//p(w)
			//pp(sheet.GetWidgets())
			//continue
		}

		if w.PassEvents() && w.HasChildren() {
			//r := w.GetAbsoluteRect()
			//p("ch.w:", w)
			//p("ch.w r:", r)
			for _, ch := range w.Children() {
				//p("ch:", ch)
				r := ch.GetAbsoluteRect()
				//p("ch.r:", ch.Rect())
				//pp("ch.ar:", r)
				if InRect(float64(mX), float64(mY), float64(r.X()), float64(r.Y()), float64(r.W()), float64(r.H())) {
					activeWidgets = append(activeWidgets, ch)
					//p("ch:", ch)
					//panic(2)
				}
				if InRect(float64(prevMx), float64(prevMy), float64(r.X()), float64(r.Y()), float64(r.W()), float64(r.H())) {
					prevActiveWidgets = append(prevActiveWidgets, ch)
					//panic(3)
				}
			}

		} else {
			if w.PassEvents() {
				continue
			}
			r := w.GetAbsoluteRect()
			if InRect(float64(mX), float64(mY), float64(r.X()), float64(r.Y()), float64(r.W()), float64(r.H())) {
				activeWidgets = append(activeWidgets, w)
				//p("w:", w)
				//panic(2)
			}
			if InRect(float64(prevMx), float64(prevMy), float64(r.X()), float64(r.Y()), float64(r.W()), float64(r.H())) {
				prevActiveWidgets = append(prevActiveWidgets, w)
				//panic(3)
			}
		}
	}
	//fmt.Println("activeWidgets:", activeWidgets)
	//fmt.Println("prevActiveWidgets:", prevActiveWidgets)

	var activeWidget WidgetI
	if len(activeWidgets) > 0 {
		activeWidget = activeWidgets[len(activeWidgets)-1]
	}
	//fmt.Println("activeWidget:", activeWidget)
	//fmt.Println("prevActiveWidget:", prevActiveWidget)

	return activeWidget
}

//func (is *InputSys) OnMouseMove(ev *ps.Event) {
//	m := ev.Data.(EvMouseMoveData)
func (is *InputSys) OnMouseMove(m EvMouseMoveData) {
	//_log.Trc("cam.onmousemove", p.X, p.Y)
	//fmt.Println("rxgui.onmousemove", p.X, p.Y)
	p("xgui.onmousemove", m.X, m.Y, m.PrevX, m.PrevY)
	p("xgui.onmousemove", m.SX, m.SY, m.PrevSX, m.PrevSY)

	is.MX = m.X
	is.MY = m.Y
	is.PrevMX = m.PrevX
	is.PrevMY = m.PrevY

	activeWidget := is.getActiveWidget()

	//fmt.Println("activeWidgets:", activeWidgets)
	//fmt.Println("prevActiveWidgets:", prevActiveWidgets)

	if activeWidget != nil {
		xgi.hasMouseFocus = true
		if activeWidget != is.activeWidget {
			is.activeWidget = activeWidget
			is.activeWidget.SetHover(true)
			if is.prevActiveWidget != nil {
				is.prevActiveWidget.SetHover(false)
			}
			is.prevActiveWidget = is.activeWidget
		}
	} else {
		xgi.hasMouseFocus = false
		is.activeWidget = nil
	}
	//fmt.Println("activeWidget:", activeWidget)
	//fmt.Println("prevActiveWidget:", prevActiveWidget)
	//panic(2)

	vars.mx = is.MX
	vars.my = is.MY

	evData := &XEvMouseMoveData{
		ActiveWidget: activeWidget,
		M:            m}
	Pub(Ev_mouse_move, evData)
}

//func (is *InputSys) OnMousePress(ev *ps.Event) {
func (is *InputSys) OnMouseButton(button uint8, pressed bool) {
	//m := ev.Data.(sdl.Keycode)
	p("xgui.onmousepress", button)
	//pp(2)

	activeWidget := is.getActiveWidget()

	if activeWidget != nil {
		w := activeWidget
		if pressed {
			is.lastPressedActiveWidget = activeWidget
		}

		if !pressed {
			if is.lastPressedActiveWidget == activeWidget {
				if button == sdl.BUTTON_LEFT {
					//p("ZZZ callback:", w.Callback())
					p("callback:", w.Callback)
					p("w:", w.Name(), w, "rect:", w.Rect(), "absrect:", w.GetAbsoluteRect())
					cb := w.Callback()
					cbPayload := w.CallbackPayload()
					if cb != nil {
						/*
							if cbPayload == nil {
								cb()
							} else {
								cb(cbPayload)
							}
						*/
						cb(cbPayload)
					}
				}
				//pp("hover")
			}
		}
	}

	evData := &EvMouseButtonData{
		ActiveWidget: activeWidget,
		Button:       button,
		Pressed:      pressed}
	Pub(Ev_mouse_button, evData)
}

func (is *InputSys) isMouseButtonPressed(button int) bool {
	sdl.PumpEvents()
	var sdlState uint32
	_, _, sdlState = sdl.GetMouseState()
	//p("Z state:", sdlState)
	//p("Z state2:", sdl.Button(sdlState))
	//fmt.Printf("Z state3: %#v, %b\n", sdlState, sdlState)
	return (sdlState & uint32(button)) > 0
}
