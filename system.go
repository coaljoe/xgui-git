package xgui

import (
	"fmt"
	"reflect"
	//"gopkg.in/fatih/set.v0"
	//"github.com/emirpasic/gods/sets/hashset"
	//"github.com/emirpasic/gods/lists/arraylist"
	//"github.com/emirpasic/gods/sets/treeset"
	//"github.com/emirpasic/gods/maps/treebidimap"
	//"container/list"
	//"github.com/cevaris/ordered_map"
)

type hasName interface {
	Name() string
}

type System struct {
	name  string
	desc  string
	elems []interface{}
}

func newSystem(name, desc string) *System {
	/*
		idComparator := func(a, b interface{}) {
			a.(hasId).Id() - b.(hasId).Id()
		}
	*/
	s := &System{
		name:  name,
		desc:  desc,
		elems: make([]interface{}, 0),
	}
	s.clearElems()
	return s
}

/*
func (s *System) OnDeserialize() {
	p("[System] OnDeserialize()")
	// Recreate elems
	s.elems = arraylist.New()
	s.clearElems()
	//panic(2)
}
*/

func (s *System) hasElem(el interface{}) bool {
	for _, v := range s.elems {
		if v == el {
			return true
		}
	}
	return false
}

func (s *System) idxElem(el interface{}) int {
	foundIndex := -1
	for i, v := range s.elems {
		if v == el {
			foundIndex = i
			break
		}
	}
	return foundIndex // -1 if not found
}

func (s *System) addElem(el interface{}) {
	if s.hasElem(el) {
		_log.Err("already have element", el, &el, "in system '"+s.name+"'")
		panic("addElem failed")
	}
	//s.elems.Add(el)
	s.elems = append(s.elems, el)
	_log.Inf("added element '" + reflect.TypeOf(el).String() + "' to system '" + s.name + "'")
}

func (s *System) removeElem(el interface{}) {
	p("have it:", s.hasElem(el))
	if !s.hasElem(el) {
		_log.Err("element not found ", el, &el, " in system '"+s.name+"'")
		fmt.Printf("%T\n", el)
		fmt.Printf("%#v\n", el)
		fmt.Printf("%p\n", el)
		p("dump:")
		dump(s.elems)
		panic("removeElem failed")
	}
	idx := s.idxElem(el)
	if idx == -1 {
		pp("can't index the element")
	}
	//delete(s.elems, el)
	// Delete
	s.elems = append(s.elems[:idx], s.elems[idx+1:]...)
	_log.Inf("removed element '" + reflect.TypeOf(el).String() + "' from system '" + s.name + "'")
}

func (s *System) getElems() []interface{} {
	return s.elems
}

func (s *System) size() int {
	return len(s.elems)
}

func (s *System) setElems(elems []interface{}) {
	p("DERP")
	fmt.Printf("s: %T %#v\n", s, s)
	//s.elems.Clear()
	s.clearElems()
	p("DERP2")
	p("elems size:", s.size())
	for _, el := range elems {
		fmt.Printf("el: %T %#v\n", el, el)
		//s.addElem(el)
		s.elems = append(s.elems, el)
	}
	p("elems size:", s.size())
}

func (s *System) clearElems() {
	//s.elems.Clear()
	s.elems = make([]interface{}, 0)
}

func (s *System) listElems() {
	p("list elements:")
	for n, el := range s.getElems() {
		name := "unknown"
		if xel, ok := el.(hasName); ok {
			name = xel.Name()
		}
		p(" -> %d: %s\n", n, name)
	}
	p("end")
}
