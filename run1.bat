@echo off
setlocal

set DEBUG=1
set LOGLEVEL=DEBUG

@rem export GOPATH=$HOME/dev/go
@rem #export TMPDIR=~/build/tmp

go install -v rx rx/rxgui

go run -v %1

endlocal