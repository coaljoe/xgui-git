package main

import (
	xg "bitbucket.org/coaljoe/xgui"
	gb "bitbucket.org/coaljoe/xgui/backend/gl"
	//"fmt"
)

func main() {
	println("main()")
	app := gb.NewGlApp()
	app.Init(800, 600, false)

	xgi := xg.NewXGui(app.Backend(), nil)
	_ = xgi

	//mySheet := newMySheet()
	//gui.SheetSys.AddSheet(mySheet)

	//mySheet := xgi.SheetSys.GetDefaultSheet()
	mySheet := xgi.SheetSys.CreateSheet("test")
	xgi.SheetSys.AddSheet(mySheet)

	debugText := xg.NewLabel(xg.Pos{0, 20}, "debugText")
	debugText.SetVisible(true)
	//debugText.SetVisible(false)
	//debugText.SetText("")
	//s.debugText
	mySheet.Root().AddChild(debugText)

	//debugText.SetText("Debug text")
	//debugText.SetVisible(true)

	for app.Step() {
		//println("step")
	}
}
