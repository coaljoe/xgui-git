var _ = `
package main

import (
	//"fmt"
	//. "ltest"
	. "math"
	"rx"
	. "bitbucket.org/coaljoe/rx/math"
	//rxgui "rx/rxgui"
)

func main() {
	println("main()")
	//rx.Init()

	rx.SetDefaultConf()
	//rx.ConfSetResPath("/home/j/dev/go/src/rx/res/") // fixme
	rx.ConfSetResPath("../res/") // fixme

	/*
		win := rx.NewWindow()
		//win.Init(640, 480)
		win.Init(1024, 576)
		app := rx.NewApp(win)
		app.Init()
		rxgui.Init()
		rxi := app.Rxi
		sce := rxi.Scene()
	*/

	rxi := rx.Init(1024, 768)
	app := rxi.App
	//sce := rxi.Scene()
	sce := rx.TestFwCreateDefaultScene()
	//gui := rxgui.NewRxGui()
	//sce := rx.CreateDefaultScene()
	var _ = sce
	//var _ = gui

	var _ = `

	/* add camera */

	//app.rxi.camera.transform(10, 10, 10,  -35.264, 45, 0)
	cam := rxi.Camera
	cam.SetZoom(6)
	//cam.Pos = rx.NewVector3(10, 20, 10)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)
	//cam.Pos = rx.NewVector3(0, 50, 0)
	//cam.Rot = rx.NewVector3(90, 0, 0)
	//cam.SetPos(Vec3{-5, 5, -5})
	cam.SetPos(Vec3{5, 5, 5})
	//cam.Rot = rx.NewVector3(90+35.264, 45, 0)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)

	cam.SetTarget(Vec3{0, 0, 0})

	println(cam.Pos().String())
	println(cam.Rot().String())

	/* add light */

	l1 := sce.AddLightNode("light1")
	println("l1 pos:", l1.Pos().String())
	`
	/* add light */

	//l1 := sce.AddLightNode("light1")
	//println("l1 pos:", l1.Pos().String())

	sl := rx.NewSceneLoader()
	_ = sl

	//sl.Load("res/models/primitives/triangle/triangle.dae")
	//sl.Load("res/models/primitives/box/box.dae")
	sl.Load("../res/models/primitives/box/box.dae")
	sl.Spawn()

	v := 0.0
	vInc := 50.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			sce.GetNodeByName("box").SetRot(Vec3{0, 0, v})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}

		/*
			println("GUI RENDER")
			r := rxi.Renderer()
			r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
			r.Set2DMode()
			rx.DrawLine(0, 0, 0, .5, .5, 0)
			//rx.DrawLine(0, 0, 0, 1, 1, 0)
			//rx.DrawLine(0, 0, 0, 100, 100, 0)
			r.Unset2DMode()
		*/
	}

	/*
		for app.Step() {
			//println("step")
			//dt := app.GetDt()
		}
	*/

	rx.TestFwDeinit()
	println("exiting...")
}
`
