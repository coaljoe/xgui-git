// author: Jacky Boen

package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"os"
	xg "bitbucket.org/coaljoe/xgui"
	sb "bitbucket.org/coaljoe/xgui/backend/sdl"
)

var winTitle string = "Go-SDL2 Texture"
var winWidth, winHeight int32 = 800, 600
var imageName string = "test.png"

func run() int {
	var window *sdl.Window
	var renderer *sdl.Renderer
	var texture *sdl.Texture
	var src, dst sdl.Rect
	var err error

	window, err = sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return 1
	}
	defer window.Destroy()

	renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return 2
	}
	defer renderer.Destroy()


	xgi := xg.NewXGui(app.Renderer())
	//sb := sb.NewSdlRenderer()
	sb := sb.NewSdlBackend()
	_ = sb

	sb.Init(renrerer)
	xgi.SetBackend(sb)


	image, err := img.Load(imageName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		return 3
	}
	defer image.Free()

	texture, err = renderer.CreateTextureFromSurface(image)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		return 4
	}
	defer texture.Destroy()

	src = sdl.Rect{0, 0, 512, 512}
	dst = sdl.Rect{100, 50, 512, 512}

	renderer.Clear()
	renderer.SetDrawColor(255, 0, 0, 255)
	renderer.FillRect(&sdl.Rect{0, 0, int32(winWidth), int32(winHeight)})
	renderer.Copy(texture, &src, &dst)


	//mySheet := newMySheet()
	//gui.SheetSys.AddSheet(mySheet)

	//mySheet := xgi.SheetSys.GetDefaultSheet()
	mySheet := xgi.SheetSys.CreateSheet("test")
	xgi.SheetSys.AddSheet(mySheet)

	debugText := xg.NewPanel(xg.Rect{0, 20, 100, 100})
	debugText.Label().SetVisible(false)
	debugText.Label().SetText("")
	//s.debugText
	mySheet.Root().AddChild(debugText)

	debugText.Label().SetText("Debug text")
	debugText.Label().SetVisible(true)
	

	renderer.Present()

	sdl.Delay(2000)

	return 0
}

func main() {
	os.Exit(run())
}
