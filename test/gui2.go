var _ = `
package main

import (
	//"fmt"
	//. "ltest"

	"fmt"
	. "math"
	"rx"
	. "bitbucket.org/coaljoe/rx/math"
	rg "rx/rxgui"
)

type MySheet struct {
	*rg.Sheet
}

func newMySheet() *MySheet {
	return &MySheet{
		Sheet: rg.NewSheet("MySheet"),
	}
}

func (s *MySheet) Render(r *rx.Renderer) {
	// Call base
	s.Sheet.Render(r)

	//println("MYSHEET RENDER")
	//r := rxi.Renderer()

	r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
	r.Set2DMode()
	rx.DrawLine(0, 0, 0, .5, .5, 0)
	//rx.DrawLine(0, 0, 0, 1, 1, 0)
	//rx.DrawLine(0, 0, 0, 100, 100, 0)
	r.Unset2DMode()

}

func (s *MySheet) Update(dt float64) {
	// Call base
	s.Sheet.Update(dt)
}

/*
func TestCallback(ev rxgui.Event) {

}
*/

func TestCallback2() {
	panic("test callback2")
}

func main() {
	println("main()")
	//rx.Init()

	rx.SetDefaultConf()
	//rx.ConfSetResPath("/home/j/dev/go/src/rx/res/") // fixme
	rx.ConfSetResPath("../res/") // fixme

	rxi := rx.Init(1024, 768)
	app := rxi.App
	//sce := rxi.Scene()
	sce := rx.TestFwCreateDefaultScene()
	gui := rg.NewRxGui()
	//sce := rx.CreateDefaultScene()
	var _ = sce
	//var _ = gui
	//gui.SetEnabled(false)

	mySheet := newMySheet()
	gui.SheetSys.AddSheet(mySheet)

	// Panel test
	//p := rg.NewPanel(rg.Rect{0, 0, 100, 100})
	//p := rg.NewPanel(rg.Rect{0, 0, 10, 10})
	//p := rg.NewPanel(rg.Rect{0, 0, .2, .2})
	p := rg.NewPanel(rg.Rect{.5, .5, .5, .5})
	p.SetName("blue panel")
	p.SetColor(&rg.ColorBlue)
	//p.SetCallback(rg.OnMouseClick, testCallback)
	//mySheet.AddWidget(p)
	mySheet.Root().AddChild(p)

	// Panel child test
	//p2 := rg.NewPanel(rg.Rect{0, 0, 1, 1})
	p2 := rg.NewPanel(rg.Rect{.5, .5, .5, .5})
	//p2 := rg.NewPanel(rg.Rect{0, 0, .3, .3})
	//p2 := rg.NewPanel(rg.Rect{0, 0, .1, .1})
	p2.SetName("green panel")
	p2.SetColor(&rg.ColorGreen)
	//p.AddChild(p2)

	// Extra
	//p3 := rg.NewPanel(rg.Rect{.5, .5, .5, .5})
	//p2 := rg.NewPanel(rg.Rect{0, 0, .3, .3})
	p3 := rg.NewPanel(rg.Rect{0, 0, .1, .1})
	//p3 := rg.NewPanel(rg.Rect{0, 0, rg.Px(100), rg.Px(100)})
	p3.SetName("red panel")
	//p3.SetColor(&rg.ColorRed)
	p3.SetColor(nil)
	//p.AddChild(p3)
	//p2.AddChild(p3)
	mySheet.Root().AddChild(p3)

	/*
		fmt.Println("derp")
		for _, c := range p.GetAllChildren() {
			//for _, c := range mySheet.Root().GetAllChildren() {
			fmt.Println("c:", c.Name(), c)
		}
		panic(2)
	*/

	t1 := rg.NewLabel(rg.Pos{.5, .25}, "TestText")
	mySheet.Root().AddChild(t1)
	t1.SetCallback(TestCallback2)

	/* add light */

	//l1 := sce.AddLightNode("light1")
	//println("l1 pos:", l1.Pos().String())

	sl := rx.NewSceneLoader()
	_ = sl

	//sl.Load("res/models/primitives/triangle/triangle.dae")
	//sl.Load("res/models/primitives/box/box.dae")
	sl.Load("../res/models/primitives/box/box.dae")
	sl.Spawn()

	v := 0.0
	vInc := 50.0
	rotate := true
	for app.Step() {
		//println("step")
		dt := app.GetDt()

		if rotate {
			v = Mod(v+vInc*dt, 360)
			//sce.GetNode("turret").SetRot(Vec3{0, 0, v})
			sce.GetNodeByName("box").SetRot(Vec3{0, 0, v})
			//sce.GetNode("triangle").SetRot(Vec3{v, 0, 0})
		}

		t1.SetText(fmt.Sprintf("TestText %f, %f", gui.InputSys.Mouse.SX,
			gui.InputSys.Mouse.SY))

		/*
			println("GUI RENDER")
			r := rxi.Renderer()
			r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
			r.Set2DMode()
			rx.DrawLine(0, 0, 0, .5, .5, 0)
			//rx.DrawLine(0, 0, 0, 1, 1, 0)
			//rx.DrawLine(0, 0, 0, 100, 100, 0)
			r.Unset2DMode()
		*/
	}

	/*
		for app.Step() {
			//println("step")
			//dt := app.GetDt()
		}
	*/

	rx.TestFwDeinit()
	println("exiting...")
}
`
