package main

import (
	xg "bitbucket.org/coaljoe/xgui"
	sb "bitbucket.org/coaljoe/xgui/backend/sdl"
	//"fmt"
)

func main() {
	println("main()")
	app := sb.NewSdlApp()
	app.Init(800, 600, false)

	xgi := xg.NewXGui(app.Backend(), nil)
	_ = xgi

	for app.Step() {
		//println("step")
	}
}
