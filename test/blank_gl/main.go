package main

import (
	xg "bitbucket.org/coaljoe/xgui"
	gb "bitbucket.org/coaljoe/xgui/backend/gl"
	//"fmt"
)

func main() {
	println("main()")
	app := gb.NewGlApp()
	app.Init(800, 600, false)

	xgi := xg.NewXGui(app.Backend(), nil)
	_ = xgi

	for app.Step() {
		//println("step")
	}
}
