package main

import (
	xg "bitbucket.org/coaljoe/xgui"
	sb "bitbucket.org/coaljoe/xgui/backend/sdl"
	//"fmt"
)

func main() {
	println("main()")
	app := xg.NewApp()
	app.Init(800, 600, false)

	app := sb.NewSdlApp()
	app.Init(800, 600, false)

	//xgi := xg.NewXGui(app.Backend())
	xgi := xg.NewXGui(app.Renderer())
	//sb := sb.NewSdlRenderer()
	sb := sb.NewSdlBackend()
	_ = sb

	xgi.SetBackend(sb)

	//mySheet := newMySheet()
	//gui.SheetSys.AddSheet(mySheet)

	//mySheet := xgi.SheetSys.GetDefaultSheet()
	mySheet := xgi.SheetSys.CreateSheet("test")
	xgi.SheetSys.AddSheet(mySheet)

	debugText := xg.NewPanel(xg.Rect{0, 20, 100, 100})
	debugText.Label().SetVisible(false)
	debugText.Label().SetText("")
	//s.debugText
	mySheet.Root().AddChild(debugText)

	debugText.Label().SetText("Debug text")
	debugText.Label().SetVisible(true)

	for app.Step() {
		//println("step")
	}
}
