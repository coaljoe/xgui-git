package main

import (
	xg "bitbucket.org/coaljoe/xgui"
	sb "bitbucket.org/coaljoe/xgui/backend/sdl"
	"fmt"
)

func main() {
	println("main()")
	//app := xg.NewApp()
	//app.Init(800, 600, false)

	app := sb.NewSdlApp()
	app.Init(800, 600, false)

	xgi := xg.NewXGui(app.Backend(), nil)

	println(xgi)
	//panic(2)
	//xgi := xg.NewXGui(app.Renderer())
	//sb := sb.NewSdlRenderer()

	/*
		sb := sb.NewSdlBackend()
		_ = sb

		xgi.SetBackend(sb)
	*/

	//mySheet := newMySheet()
	//gui.SheetSys.AddSheet(mySheet)

	//mySheet := xgi.SheetSys.GetDefaultSheet()
	mySheet := xgi.SheetSys.CreateSheet("test")
	xgi.SheetSys.AddSheet(mySheet)

	/*
		debugText := xg.NewPanel(xg.Rect{0, 20, 100, 100})
		debugText.Label().SetVisible(false)
		debugText.Label().SetText("")
		//s.debugText
		mySheet.Root().AddChild(debugText)

		debugText.Label().SetText("Debug text")
		debugText.Label().SetVisible(true)
	*/

	//debugText := xg.NewSlider(xg.Rect{0, 20, 100, 100})
	debugText := xg.NewButton(xg.Rect{0, 20, 100, 100})
	//debugText := xg.NewPanel(xg.Rect{0, 20, 100, 100})
	//debugText.Label().SetVisible(false)
	//debugText.Label().SetText("")
	//s.debugText
	mySheet.Root().AddChild(debugText)

	//debugText.Label().SetText("Debug text")
	//debugText.Label().SetVisible(true)

	if false {
		p := xg.NewPanel(xg.Rect{100, 100, 2, 100})
		p.Style.DrawAsRect = true
		p.Style.DrawRectColor = xg.ColorBlue
		mySheet.Root().AddChild(p)
	}

	if false {
		rect := xg.Rect{0, 20, 10, 10}
		debugText.SetClipRect(&rect)
		//debugText.SetClipRect(nil)
	}

	if true {
		st := debugText.Label.Style
		st.TextColor = xg.Color{0, 255, 0, 255}
		st.TextFont = xgi.Style.Fonts["text_font_bold"]

		fmt.Printf("%#v\n", (st))
		//panic(2)
	}

	if false {
		p := xg.NewPanel(xg.Rect{0, 20, 100, 100})
		st := p.Style
		stCopy := p.Style.Copy()
		fmt.Printf("st %p %p\n", st, stCopy)
		fmt.Printf("st.Style %p %p\n", st.Style, stCopy.Style)
		fmt.Println()
		st.DrawAsRect = true
		fmt.Printf("st %#v\n", st)
		fmt.Println()
		fmt.Printf("stCopy %#v\n", stCopy)
		panic(2)
	}

	debugText.Label.SetText("Line 1\nLine 2")

	i := 0
	for app.Step() {
		//println("step")

		if true {
			i += 1
			s := fmt.Sprintf("line\ni: %d", i)
			debugText.Label.SetText(s)
		}
	}
}
