package main

import (
	xg "bitbucket.org/coaljoe/xgui"
	sb "bitbucket.org/coaljoe/xgui/backend/sdl"
	"fmt"
)

func main() {
	println("main()")
	app := sb.NewSdlApp()
	app.Init(800, 600, false)

	xgi := xg.NewXGui(app.Backend(), nil)
	_ = xgi

	//mySheet := newMySheet()
	//gui.SheetSys.AddSheet(mySheet)

	//mySheet := xgi.SheetSys.GetDefaultSheet()
	mySheet := xgi.SheetSys.CreateSheet("test")
	xgi.SheetSys.AddSheet(mySheet)

	debugText := xg.NewLabel(xg.Pos{0, 20}, "debugText")
	st := debugText.Style
	//debugText.SetVisible(false)
	//debugText.SetText("")
	//s.debugText
	mySheet.Root().AddChild(debugText)

	//debugText.SetText("Debug text")
	//debugText.SetVisible(true)

	frame := 0
	freq := 50
	for app.Step() {
		println("step")

		if (frame / freq) % 2 == 0 {
			st.TextColor = xg.ColorRed
			st.TextFont = xgi.Style.Fonts["text_font_bold"]
		} else {
			st.TextColor = xg.ColorGreen
			st.TextFont = xgi.Style.Fonts["text_font"]
		}
		fmt.Println(st.TextColor)

		//app.Step()
		
		frame++
	}
}
