// author: Jacky Boen

package main

import (
	xg "bitbucket.org/coaljoe/xgui"
	sb "bitbucket.org/coaljoe/xgui/backend/sdl"
	"flag"
	"fmt"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"os"
)

var winTitle string = "Go-SDL2 Texture"
var winWidth, winHeight int32 = 800, 600
var imageName string = "test.png"

func run() int {

	flag.Bool("q", false, "test")
	flag.Parse()

	f := flag.Lookup("q")

	fmt.Printf("f: %v\n", f)
	//panic(2)

	var window *sdl.Window
	var renderer *sdl.Renderer
	var texture *sdl.Texture
	var src, dst sdl.Rect
	var err error

	window, err = sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return 1
	}
	defer window.Destroy()

	renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return 2
	}
	defer renderer.Destroy()

	//sb := sb.NewSdlRenderer()
	sb := sb.NewSdlBackend()
	_ = sb
	sb.Init(renderer)

	xgi := xg.NewXGui(sb, nil)

	image, err := img.Load(imageName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		return 3
	}
	defer image.Free()

	texture, err = renderer.CreateTextureFromSurface(image)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		return 4
	}
	defer texture.Destroy()

	src = sdl.Rect{0, 0, 512, 512}
	dst = sdl.Rect{100, 50, 512, 512}

	//mySheet := newMySheet()
	//gui.SheetSys.AddSheet(mySheet)

	//mySheet := xgi.SheetSys.GetDefaultSheet()
	mySheet := xgi.SheetSys.CreateSheet("test")
	xgi.SheetSys.AddSheet(mySheet)

	//debugText := xg.NewPanel(xg.Rect{0, 20, 100, 100})
	debugText := xg.NewButton(xg.Rect{0, 20, 100, 100})
	//debugText := xg.NewSlider(xg.Rect{0, 20, 100, 100})
	debugText.Label.SetVisible(false)
	debugText.Label.SetText("")
	//s.debugText
	mySheet.Root().AddChild(debugText)

	debugText.Label.SetText("Debug text")
	debugText.Label.SetVisible(true)

	//xgi.Update(0.1)
	//xgi.Render()
	//renderer.Present()

	//sdl.Delay(2000)

	var event sdl.Event

	running := true
	for running {

		//event := sdl.WaitEvent()
		for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				running = false

			//case *sdl.KeyUpEvent:
			//	fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n",
			//		t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)

			case *sdl.KeyboardEvent:
				fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n",
					t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)
				if t.Keysym.Sym == 'q' {
					running = false
				}

			case *sdl.MouseMotionEvent:
				fmt.Printf("[%d ms] MouseMotion\tid:%d\tx:%d\ty:%d\txrel:%d\tyrel:%d\n", t.Timestamp, t.Which, t.X, t.Y, t.XRel, t.YRel)

				xgi.InjectMouseMove(int(t.X), int(t.Y))
				//panic(2)

			case *sdl.MouseButtonEvent:

				//pub(ev_mouse_button_event, t)
				if t.State == sdl.PRESSED {
					btn := int(t.Button)
					xgi.InjectMouseButton(btn, true)
				} else if t.State == sdl.RELEASED {
					btn := int(t.Button)
					xgi.InjectMouseButton(btn, false)
				}

				//panic(2)

			}
		}

		// Draw
		renderer.Clear()
		renderer.SetDrawColor(255, 0, 0, 255)
		renderer.FillRect(&sdl.Rect{0, 0, int32(winWidth), int32(winHeight)})
		renderer.Copy(texture, &src, &dst)

		xgi.Update(0.1)
		xgi.Render()

		renderer.Present()
	}

	return 0
}

func main() {
	os.Exit(run())
}
