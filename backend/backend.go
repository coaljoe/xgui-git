package backend

import (
	//"github.com/veandco/go-sdl2/sdl"
	//"bitbucket.org/coaljoe/xgui/backend/sdl"
	. "bitbucket.org/coaljoe/xgui/types"
)

type XGuiInstanceI interface {
	//Draw()
	Render()
	Update(dt float64)
	InjectMouseButton(btn int, pressed bool)
	InjectMouseMove(x, y int)
}

type BackendI interface {
	//Renderer() sdl.Renderer
	//Renderer() *sdl.Renderer
	Renderer() RendererI
	Register(x XGuiInstanceI)
	TextRenderer() TextRendererI
	NewImage() ImageI
	NewFont() FontI
}

type RendererI interface {
	DrawRect(x, y, w, h int, c Color)
	SetDrawColor(r, g, b, a int)
	DrawLine(x1, y1, x2, y2 int)
	SetClipRect(x, y, w, h int)
	UnsetClipRect()
	HasClipRect() bool
	Size() (int, int)
	PreRender()
	PostRender()
}

// XXX move to xgui?
type ImageI interface {
	W() int
	H() int
	Load(path string)
	Build()
	Create(w, h int)
	PutPixel(x, y, r, g, b int, update bool)
	HasImage() bool
	ScaleFilter() bool
	SetScaleFilter(v bool)
	Free()
	Render(x, y, w, h int)
	RenderScale(x, y, w, h int, sx, sy float64)
	//RenderRect(srcrect sdl.Rect, dstrect sdl.Rect)
	RenderRect(srcrect Rect, dstrect Rect)
}

type FontI interface {
	Load(path string, size int)
	AA() bool
	SetAA(v bool)
}

type TextRendererI interface {
	//RenderText(text string, x, y int, font FontI)
	RenderTextColor(text string, x, y int, color Color, font FontI)
	RenderTextColorAsImage(text string, tw, th int, color Color, font FontI) ImageI
	SizeText(text string, font FontI) (int, int)
}
