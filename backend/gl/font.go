package glbackend

import (
	"fmt"
	"os"

	"bitbucket.org/coaljoe/xgui/backend/gl/text"
)

type Font struct {
	path string
	size int
	loaded bool
	aa bool
	tFont *text.Font
}

func NewFont() *Font {
	f := &Font{
		aa: true,	
	}
	return f
}

func (f *Font) Load(path string, size int) {
	var font *text.Font
	var err error
	

	if _, err = os.Stat(path); os.IsNotExist(err) {
		fmt.Printf("error: font doesn't exist; fontPath: %s\n", path)
		panic(err)
	}
	if font, err = text.NewFont(path); err == nil {
		//text.DefaultFont = font
	} else {
		fmt.Printf("Failed to open font: %s\n", err)
		panic(err)
	}
	if false {
	fa := text.FontAttributes{}
	//fa.PointSize = 60
	fa.PointSize = float64(size)
	fa.DPI = 72
	//fa.Hinting = text.HintingNone
	fa.Hinting = text.HintingFull
	fa.LineSpacing = 1.0
	//text.DefaultFontAttributes = fa
	}
	
	attr := text.DefaultFontAttributes
	attr.PointSize = float64(size)
	font.SetAttributes(&attr)
	
	f.tFont = font
	f.size = size
	f.path = path
	f.loaded = true
	
	f.tFont = font
}

func (f *Font) AA() bool {
	return f.aa
}

func (f *Font) SetAA(v bool) {
	f.aa = v	
}
