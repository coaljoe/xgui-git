package glbackend

import (
	//ps "bitbucket.org/coaljoe/lib/pubsub"
	"fmt"
	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
	//xg "bitbucket.org/coaljoe/xgui"
	"bitbucket.org/coaljoe/xgui/backend"
	//"os"
)

type GlApp struct {
	//*xg.App // BaseApp
	*backend.BaseApp
	title string
	window     *glfw.Window
	//renderer *sdl.Renderer
	gb *GlBackend
}

func NewGlApp() *GlApp {
	a := &GlApp{
		BaseApp: backend.NewBaseApp(),
		title: "GlBackend Window",
	}
	return a
}

/*
func (a *GlApp) Renderer() backend.RendererI {
	//return a.renderer
	//return a.gb.r.br
	return nil
}
*/

func (a *GlApp) Backend() *GlBackend {
	return a.gb
}



func (a *GlApp) Init(width, height int, fullscreen bool) {
	println("GlApp init")
	a.BaseApp.Init(width, height, fullscreen)

	title := a.title
	
	
	
	// check args
	if width < 1 || height < 1 {
		panic("bad width and height")
	}

	/*
	w.width = width
	w.height = height
	w.fullscreen = fullscreen
	*/

	/* Init GLFW */

	err := glfw.Init()
	if err != nil {
		println("Can't init glfw!")
		panic(err)
	}

	glfw.WindowHint(glfw.ContextVersionMajor, 2)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)

	//glfwWindowHint(GLFW_WINDOW_NO_RESIZE, true)
	//glfwWindowHint(glfw.FSAA_SAMPLES, 8)
	//glfw.WindowHint(glfw.Samples, 0)
	//glfw.WindowHint(glfw.Samples, 2)
	//glfw.WindowHint(glfw.Samples, 8)
	glfw.WindowHint(glfw.Samples, glfw.DontCare) // use os settings
	glfw.WindowHint(glfw.Resizable, gl.FALSE)
	glfw.WindowHint(glfw.Focused, gl.TRUE)

	//if Debug {
	//	//GLFW_OPENGL_DEBUG_CONTEXT
	//	glfw.WindowHint(glfw.OpenGLDebugContext, gl.TRUE)
	//}

	var monitor *glfw.Monitor
	if fullscreen {
		monitor = glfw.GetPrimaryMonitor()
	}

	glfw.WindowHint(glfw.RedBits, 8)
	glfw.WindowHint(glfw.GreenBits, 8)
	glfw.WindowHint(glfw.BlueBits, 8)
	glfw.WindowHint(glfw.AlphaBits, 0)
	glfw.WindowHint(glfw.DepthBits, 24)
	glfw.WindowHint(glfw.StencilBits, 0)

	window, err := glfw.CreateWindow(width, height, title, monitor, nil)
	if err != nil {
		panic(err)
	}
	a.window = window
	
	// glfw context.
	window.MakeContextCurrent()

	var _ = `
	// Input callbacks.
	//glfw.SetMouseButtonCallback(w.glfwWin, w.on_mouse_button)
	//glfw.SetScrollCallback(w.glfwWin, w.on_mouse_scroll)
	//glfw.SetCursorPosCallback(w.glfwWin, w.on_mouse_pos)
	window.SetKeyCallback(_InputSys.on_key)
	window.SetCursorPosCallback(_InputSys.on_mouse_move)
	window.SetMouseButtonCallback(_InputSys.on_mouse_button)
	window.SetFocusCallback(w.on_focus)
	window.SetCursorEnterCallback(w.on_cursor_enter)
	`

	glfw.SwapInterval(1) // vsync
	//glfw.SetWindowPos(w.glfwWin, 100, 100)
	//glfw.Disable(glfw.MOUSE_CURSOR)

	/* Init GL */

	// have to call this here or some OpenGL calls like CreateProgram will cause segfault
	if err := gl.Init(); err != nil {
		println("GL init failed. (Can't init GLEW(?))")
		panic(err)
	}
	// important?
	//gl.Enable(gl.MULTISAMPLE)     // xxx: not tested / don't needed?
	//gl.Enable(gl.MULTISAMPLE_ARB) // xxx: not tested / don't needed?
	gl.Viewport(0, 0, int32(width), int32(height))
	//gl.ClearColor(0.0, 0.0, 0.0, 1.0)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()

	// lock cursor
	//w.updateCursorLock()
	/*
		  XXX not working: X11 async bug
		  // center cursor
		  w.glfwWin.SetCursorPos(float64(w.width/2), float64(w.height/2))
		  fmt.Println(w.glfwWin.GetCursorPos())
			panic(2)
	*/
	//return true
	
	//// Setup
	gl.Enable(gl.DEPTH_TEST)
	gl.Enable(gl.TEXTURE_2D)

	gb := NewGlBackend()
	//gb.Init(renderer)
	gb.Init()
	a.gb = gb

	//a.r = NewSdlRenderer()
	//a.r.Init(renderer)
	println("done GlApp init")
}

func (a *GlApp) ProcessEvents() {

	//println("main.step")
	/*
		renderer.Clear()
		renderer.SetDrawColor(255, 0, 0, 255)
		renderer.FillRect(&sdl.Rect{0, 0, int32(winWidth), int32(winHeight)})
		//renderer.Copy(texture, &src, &dst)
		renderer.Present()
	*/

	if !a.window.ShouldClose() {
		glfw.PollEvents()
	} else {
		a.Quit()
	}
}

func (a *GlApp) Draw() {
	println("GlApp draw")
	//renderer := a.Renderer()

	// Reset Color
	//renderer.SetDrawColor(0, 0, 0, 255)
	// Clear
	//renderer.Clear()
	
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	
	gr := a.gb.Renderer()
	_ = gr
	gr.PreRender()
	gr.DrawLine(0, 0, 10, 10)
	gr.PostRender()

	// Draw gui here
	//xgi.Render(renderer)

	fmt.Printf("xgi: %#v\n", a.gb.xgi)

	a.gb.xgi.Render()

	//renderer.Present()
	a.window.SwapBuffers()
}

func (a *GlApp) Step() bool {
	println("GlApp step")
	if !a.Running() {
		a.Quit()
		return false
	}

	a.PreStep()

	a.ProcessEvents()


	a.Update(a.Dt())
	a.Draw()

	a.PostStep()

	return true
}

func (a *GlApp) Quit() {
	println("App.Quit")
	a.SetRunning(false)
}

func (a *GlApp) Update(dt float64) {
	a.gb.xgi.Update(dt)
}
