package glbackend

import (
	"fmt"
	"image"
	"image/draw"
	"github.com/go-gl/gl/v2.1/gl"
	. "bitbucket.org/coaljoe/xgui/types"
	"bitbucket.org/coaljoe/xgui/backend"
	"strings"
)

// Gl text rendering backend
type TextRenderer struct {
}

func NewTextRenderer() *TextRenderer {
	tr := &TextRenderer{
	}
	return tr
}

func (tr *TextRenderer) RenderTextColor(text string, x, y int, color Color, font backend.FontI) {
	tr.renderTextColor(text, x, y, color, font, nil)
}

// Creates a new image
func (tr *TextRenderer) RenderTextColorAsImage(text string, tw, th int, color Color, font backend.FontI) backend.ImageI {
	println("RenderTextColorAsImage text:", text)
	
	im := NewImage()
	im.Create(tw, th)

	x := 0
	y := 0
	_ = x
	_ = y
	
	img := image.NewRGBA(image.Rect(0, 0, tw, th))

	//surf.Free()

	//return nil

	/*
	// Fill textures pixel values with text's color to avoid issues
	// with font anti-aliasing
	// https://forums.libsdl.org/viewtopic.php?p=51795
	//c := sdl.MapRGBA(surf.Format, uint8(color[0]), uint8(color[1]), uint8(color[2]), uint8(color[3]))
	c := sdl.MapRGBA(surf.Format, uint8(color[0]), uint8(color[1]), uint8(color[2]), 0)
	surf.FillRect(nil, c)
	*/
	
	tr.renderTextColor(text, x, y, color, font, img)
	savePng("/tmp/t2.png", img)
	
	im.setTexData(img.Pix)

	im.Build()
	//im.Build2()

	//im.surf = nil
	//surf.Free()

	//return nil

	// XXX cleanup
	img = nil
	
	//fmt.Printf("%#v\n", im)
	//panic(2)
	return im
}


func (tr *TextRenderer) renderTextColor(text string, x, y int, color Color, font backend.FontI, targetImg *image.RGBA) {
	fmt.Println("renderTextColor text:", text, "x:", x, "y:", y, "color:", color, "font:", fmt.Sprintf("%p", font),
		"targetImg:", fmt.Sprintf("%p", targetImg))
	//tr.renderLine(text, x, y, color, font)
	//return

	lines := strings.Split(text, "\n")
	ysh := 0
	
	// XXX calculate height of arbitrary string, fixme?
	_, yshInc := tr.sizeLine("1", font)

	for _, line := range lines {
		if targetImg == nil {
			tr.renderLine(line, x, y + ysh, color, font, nil)
		} else {
			tr.renderLine(line, x, y + ysh, color, font, targetImg)
		}
		//ysh += font.Size()
		//ysh += 20
		ysh += yshInc
	}
}

func (tr *TextRenderer) renderLine(text string, x, y int, color Color, font backend.FontI, targetImg *image.RGBA) {
	if text == "" {
		return
	}
	
	if font.AA() {
	} else {
		println("non AA fonts not supported")
		panic("not implemented")
	}
	
	text = "Test"
	
	// XXX hacky, fixme?
	tFont := font.(*Font).tFont
	
	// Create an image with the text
	textImage := tFont.DrawText(text)
	savePng("/tmp/t.png", textImage)
	
	b := textImage.Bounds()
	imgWidth := b.Max.X
	imgHeight := b.Max.Y
	//tex := NewEmptyTexture(imgWidth, imgHeight, TextureFormatRGBA)
	tmpImg := NewImage()
	tmpImg.Create(imgWidth, imgHeight)
	tex := tmpImg.tex
	
	tmpImg.setTexData(textImage.Pix)


	if targetImg != nil {
		draw.Draw(targetImg, textImage.Bounds(), textImage, image.Point{0, 0}, draw.Src)
	} else {

		gl.Enable(gl.BLEND)
		gl.Disable(gl.DEPTH_TEST)
		
		w := imgWidth
		h := imgHeight
		
		drawQuadTexFlipY(x, y, w, h, ColorWhite, tex)
		
		gl.Enable(gl.DEPTH_TEST)
		gl.Disable(gl.BLEND)
		
		// Destroy texture
		tmpImg.Free()
		glcheck()
	}

	// Cleanup
	tmpImg.Free()
}

func (tr *TextRenderer) SizeText(text string, font backend.FontI) (int, int) {
	//return tr.sizeLine(text, font)

	//s := strings.TrimRight(text, "\n")
	s := text
	//println("XXX s:", s)
	lines := strings.Split(s, "\n")
	//println("len:", len(lines))
	//println("0 ", lines[0])
	//println("1 ", lines[1])

	// XXX hacky, fixme?
	tFont := font.(*Font).tFont
	// Recommended line height
	//lineH := tFont.LineSkip()
	metrics := tFont.Metrics()
	lineH := (metrics.Ascent + metrics.Descent).Ceil()
	
	sx, sy := 0, 0
	for i, line := range lines {
		_ = i
		//println("derp")
		//println("->", i, line)
		w, h := tr.sizeLine(line, font)

		if w > sx {
			sx = w
		}
		
		_ = h
		//sy += 10
		//sy += h
		sy += lineH
		//sy += 20 // font.Size()
	}

	return sx, sy
}

func (tr *TextRenderer) sizeLine(text string, font backend.FontI) (int, int) {
	// XXX hacky, fixme?
	tFont := font.(*Font).tFont

	//w, h, err := tr.font.SizeUTF8(text)
	w, h := tFont.MeasureText(text)
	return w, h
}

var _ = `
func (tr *TextRenderer) RenderTextColor(text string, x, y float64, color Color) {

	sx := 2.0 / float64(rxi.Renderer().Width())
	sy := 2.0 / float64(rxi.Renderer().Height())

	x = -1.0 + (x * 2)
	y = (y * 2) - 1.0

	//p(tr.font.ft_font.MaxAdvanceHeight)
	//pp(tr.font.ft_font.BBox)
	//yAdj := float64(tr.font.ft_font.MaxAdvanceHeight/64) * rxi.Renderer().GetPh()
	//yAdj := float64(tr.font.ft_font.Ascender/64) * sy
	yAdj := float64(tr.font.Height()*2) * rxi.Renderer().GetPh()
	//yAdj := float64(3442/64) * rxi.Renderer().GetPh()
	y += yAdj
	//pp(float64(tr.font.ft_font.LineHeight/64) * rxi.Renderer().GetPh())

	// FIXME: text rendering doesn't work with culling enabled
	// disable culling for whole GUI pass?
	prevCullEnabled := false
	if gl.IsEnabled(gl.CULL_FACE) {
		prevCullEnabled = true
		gl.Disable(gl.CULL_FACE)
	}

	prevBlendEnabled := false
	if gl.IsEnabled(gl.BLEND) {
		prevBlendEnabled = true
	} else {
		gl.Enable(gl.BLEND)
		gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	}
	gl.BindTexture(gl.TEXTURE_2D, tr.gl_tex)

	tr.shader.Bind()
	tr.shader.PassUniform1i("tex", 0)
	tr.shader.PassUniform4f_("color", color.ToVec4())

	gl.EnableVertexAttribArray(uint32(tr.gl_attribute_coord))
	gl.BindBuffer(gl.ARRAY_BUFFER, tr.gl_vbo)
	gl.VertexAttribPointer(uint32(tr.gl_attribute_coord), 4, gl.FLOAT,
		false, 0, gl.PtrOffset(0))

	for runeIdx, rune := range text {
		//c := text[i]
		//fmt.Printf("rune: %#U\n", rune)

		skipRendering := false
		if unicode.IsSpace(rune) {
			skipRendering = true
		}

		font := tr.font.ft_font
		gIndex := font.Index(rune)
		//p("index:", gIndex)
		g, err := font.Load(gIndex)
		if err != nil {
			panic(fmt.Sprintf(
				"Failed to load glyph: %#U at byte position %d",
				rune, runeIdx))
		}
		gImage, err := g.Image()
		if err != nil {
			panic(err)
		}
		_ = gImage

		// Size in pixels
		gWidth := g.Width / 64
		gHeight := g.Height / 64

		//assert(gImage.ColorModel() == _color.AlphaModel)
		//p(len(gImage.Pix), g.Width, g.Height, gImage.Bounds())
		//pp(2)

		if false && !unicode.IsSpace(rune) {
			outFile, err := os.Create("/tmp/rxgui_test_freetype_out.png")
			if err != nil {
				panic(err)
			}

			err = png.Encode(outFile, gImage)
			if err != nil {
				panic(err)
			}
			println("wrote /tmp/rxgui_test_freetype_out.png file")
		}

		if !skipRendering {
			// Upload the glyph's image
			gl.TexImage2D(gl.TEXTURE_2D, 0, gl.ALPHA,
				int32(gWidth), int32(gHeight),
				0, gl.ALPHA, gl.UNSIGNED_BYTE, gl.Ptr(gImage.Pix))
		}

		// Calculate vertex and texture coord
		bitmap_left := float32(g.HMetrics.BearingX / 64)
		//bitmap_top := float32(-g.VMetrics.BearingY / 64)
		bitmap_top := float32(g.VMetrics.BearingY / 64)
		bitmap_width := float32(gWidth)
		bitmap_rows := float32(gHeight)

		x2 := float32(x) + bitmap_left*float32(sx)
		y2 := float32(-y) + bitmap_top*float32(sy)
		w := bitmap_width * float32(sx)
		h := bitmap_rows * float32(sy)

		// x, y, s, t
		box := []float32{
			x2, -y2, 0, 0,
			x2 + w, -y2, 1, 0,
			x2, -y2 - h, 0, 1,
			x2 + w, -y2 - h, 1, 1,
		}
		//p(box)

		// Draw the character on the screen
		gl.BufferData(gl.ARRAY_BUFFER, 16*4, gl.Ptr(&box[0]), gl.DYNAMIC_DRAW)
		//gl.BufferData(gl.ARRAY_BUFFER, 16*4, gl.Ptr(box), gl.DYNAMIC_DRAW)
		gl.DrawArrays(gl.TRIANGLE_STRIP, 0, 4)

		// Advance the cursor to the start of the next character
		advance_x := g.HMetrics.Advance / 64
		//pp(advance_x, float64(advance_x)*sx)
		//advance_y := g.VMetrics.Advance / 64
		//p(advance_y, float64(advance_y)*sy)
		//x += float64(advance_x>>6) * sx
		//y += float64(advance_y>>6) * sy
		//x += 0.05
		x += float64(advance_x) * sx
		//y += float64(advance_y) * sy
		y += 0
	}

	gl.DisableVertexAttribArray(uint32(tr.gl_attribute_coord))
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindTexture(gl.TEXTURE_2D, 0)
	tr.shader.Unbind()

	if !prevBlendEnabled {
		gl.Disable(gl.BLEND)
	}

	if prevCullEnabled {
		gl.Enable(gl.CULL_FACE) // Restore culling
	}

	rx.Xglcheck()
}
`
