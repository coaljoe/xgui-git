package glbackend

import (
	"fmt"
	"os"
	"image"
	"image/png"
	_debug "runtime/debug"
	"github.com/go-gl/gl/v2.1/gl"
    "bitbucket.org/coaljoe/lib/debug"

)

var Debug = true

var p = debug.P
var pp = debug.Pp

var __p = p
var _ = __p

var __pp = pp
var _ = __pp


func trace(s string) {
	fmt.Println("\nTrace:\n" + s)
	_debug.PrintStack()
	panic(s)
}

func glcheck() {
	//return
	if !Debug {
		return
	}

	if err := gl.GetError(); err != gl.NO_ERROR {
		switch err {
		case gl.INVALID_ENUM:
			trace("OpenGL Error: GL_INVALID_ENUM")
		case gl.INVALID_VALUE:
			trace("OpenGL Error: GL_INVALID_VALUE")
		case gl.INVALID_OPERATION:
			trace("OpenGL Error: GL_INVALID_OPERATION")
		case gl.STACK_OVERFLOW:
			trace("OpenGL Error: GL_STACK_OVERFLOW")
		case gl.STACK_UNDERFLOW:
			trace("OpenGL Error: GL_STACK_UNDERFLOW")
		case gl.OUT_OF_MEMORY:
			trace("OpenGL Error: GL_OUT_OF_MEMORY")
		}
	}

	// gl.NO_ERROR
}

func savePng(path string, img *image.RGBA) {
	println("savePng path:", path)
	//panic(2)
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}

	if err := png.Encode(f, img); err != nil {
		f.Close()
		panic(err)
	}

	if err := f.Close(); err != nil {
		panic(err)
	}
}
