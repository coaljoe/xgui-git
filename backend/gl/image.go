package glbackend

import (
	//"unsafe"
	//"math"
	"github.com/go-gl/gl/v2.1/gl"
	"image"
	//"image/color"
	"image/draw"
	_ "image/png"
	"os"
	"fmt"
	. "bitbucket.org/coaljoe/xgui/types"	
)

type Image struct {
	w, h  int
	//Scale bool
	// XXX should be set before Create(!)
	scaleFilter bool
	prevSQVal   string
	// gl
	tex        uint32
	hasTex 	   bool
}

func NewImage() *Image {
	i := &Image{
		scaleFilter: false,
	}
	fmt.Printf("NewImage() (i=%p)\n", i)
	return i
}

//func (i *Image) Surf() *sdl.Surface { return i.surf }
func (i *Image) ScaleFilter() bool { return i.scaleFilter }
func (i *Image) SetScaleFilter(v bool) { i.scaleFilter = v }
func (i *Image) W() int { return i.w }
func (i *Image) H() int { return i.h }

func (i *Image) Load(path string) {
	println("image.load path:", path)

	imgFile, err := os.Open(path)
	if err != nil {
		fmt.Printf("texture %q not found on disk: %v\n", path, err)
		panic(err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		panic(err)
	}

	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		panic("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

	iw := rgba.Rect.Size().X
	ih := rgba.Rect.Size().Y

	texture := i.genTex(iw, ih)
	/*
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(iw),
		int32(ih),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix))
	*/
	

	i.w = iw
	i.h = ih
	i.hasTex = true
	i.tex = texture

	i.Build()
	
	i.setTexData(rgba.Pix)
}

func (i *Image) genTex(w, h int) uint32 {
	var texture uint32
	gl.Enable(gl.TEXTURE_2D)
	gl.GenTextures(1, &texture)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	return texture
}

func (i *Image) setTexData(pix []uint8) {
	fmt.Printf("image.setTexData (i=%p)\n", i)
	if !i.hasTex {
		panic("image wasn't created")
	}
	println("len pix:", len(pix))
	//pp(2)
	gl.BindTexture(gl.TEXTURE_2D, i.tex)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(i.w),
		int32(i.h),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(pix))
		
	glcheck()
}

func (i *Image) Build() {
	if !i.hasTex {
		panic("image wasn't created")
	}
	if i.scaleFilter {
		gl.BindTexture(gl.TEXTURE_2D, i.tex)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
		gl.BindTexture(gl.TEXTURE_2D, 0)
	} else {
		gl.BindTexture(gl.TEXTURE_2D, i.tex)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)	
		gl.BindTexture(gl.TEXTURE_2D, 0)
	}
}

func (i *Image) Create(w, h int) {
	println("image.create: w:", w, "h:", h)
	tex := i.genTex(w, h)
	i.w = w
	i.h = h
	i.hasTex = true
	i.tex = tex

	i.Build()
}

func (i *Image) PutPixel(x, y, r, g, b int, update bool) {
	panic("not implemented")
}

func (i *Image) HasImage() bool {
	return i.hasTex
}

func (i *Image) Free() {
	gl.DeleteTextures(1, &i.tex)
}

func (i *Image) Render(x, y, w, h int) {

	xw, xh := w, h
	if w == -1 && h == -1 {
		xw, xh = i.w, i.h
	}
	
	/*
	gl.BindTexture(gl.TEXTURE_2D, i.tex)
	
	gl.Begin(gl.QUADS)
	gl.TexCoord2f(0, 0)
	gl.Vertex2f(float32(x), float32(y))
	gl.TexCoord2f(1, 0)
	gl.Vertex2f(float32(x+xw), float32(y))
	gl.TexCoord2f(1, 1)
	gl.Vertex2f(float32(x+xw), float32(y+xh))
	gl.TexCoord2f(0, 1)
	gl.Vertex2f(float32(x), float32(y+xh))
	gl.End()
	
	gl.BindTexture(gl.TEXTURE_2D, 0)
	*/
	
	println(i.hasTex)
	
	if !i.hasTex {
		println("warning: image doesn't have texture")
		return
	} else {
		//panic(3)	
	}
	
	//drawQuadTex(x, y, xw, xh, ColorWhite, i.tex)
	drawQuadTex(x, y, xw, xh, ColorRed, i.tex)
	//pp(2)
}

func (i *Image) RenderScale(x, y, w, h int, sx, sy float64) {
	panic("not implemented")
}

func (i *Image) RenderRect(srcrect Rect, dstrect Rect) {
	panic("not implemented")
}
