package glbackend

import (
	//ps "bitbucket.org/coaljoe/lib/pubsub"
	//"fmt"
	//"github.com/veandco/go-sdl2/sdl"
	//"github.com/go-gl/gl/v2.1/gl"
	"bitbucket.org/coaljoe/xgui/backend"
	//"os"
)

type GlBackend struct {
	r              *GlRenderer
	tr *TextRenderer
	xgi backend.XGuiInstanceI
}

func NewGlBackend() *GlBackend {
	b := &GlBackend{
	}
	return b
}

func (gb *GlBackend) Renderer() backend.RendererI {
	return gb.r
}

func (gb *GlBackend) TextRenderer() backend.TextRendererI {
	return gb.tr
}

func (gb *GlBackend) Register(xgi backend.XGuiInstanceI) {
	gb.xgi = xgi
}

//func (gb *GlBackend) Init(renderer *sdl.Renderer) {
func (gb *GlBackend) Init() {
	println("glbackend init")

	gb.r = NewGlRenderer()
	gb.r.Init()
	
	gb.tr = NewTextRenderer()

	println("done glbackend init")
}

func (gb *GlBackend) NewImage() backend.ImageI {
	return NewImage()
}

func (gb *GlBackend) NewFont() backend.FontI {
	return NewFont()
}
