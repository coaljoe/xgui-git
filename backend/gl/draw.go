// Utilitarian draw functions
package glbackend


import (	
	"github.com/go-gl/gl/v2.1/gl"
	. "bitbucket.org/coaljoe/xgui/types"
	"fmt"
)

func _drawQuad(x, y, w, h int, c Color, useTex bool, tex uint32, flipY bool) {
	//p("_drawQuad", x, y, w, h, c, useTex, tex, flipY)
	fmt.Println("_drawQuad", x, y, w, h, c, useTex, tex, flipY)
	//pp(3)
	cv := c.ToVec4()
	gl.Color3f(float32(cv[0]), float32(cv[1]), float32(cv[2]))
	
	if useTex {
		gl.BindTexture(gl.TEXTURE_2D, tex)
	}

	// CCW
    gl.Begin(gl.QUADS)
    if useTex {
    	//gl.TexCoord2f(0, 0)
    	gl.TexCoord2i(0, 0)
    }
    gl.Vertex2i(int32(x), int32(y)) // vertex 1
    if useTex {
    	//gl.TexCoord2f(1, 0)
    	gl.TexCoord2i(int32(w), 0)
    }
    gl.Vertex2i(int32(x+w), int32(y)) // vertex 2
    if useTex {
    	//gl.TexCoord2f(1, 1)
       	gl.TexCoord2i(int32(w), int32(h))
    }
    gl.Vertex2i(int32(x+w), int32(y+h)) // vertex 3
    if useTex {
    	//gl.TexCoord2f(0, 1)
    	gl.TexCoord2i(0, int32(h))
    }
    gl.Vertex2i(int32(x), int32(y+h)) // vertex 4
    gl.End()
    
    if useTex {
		gl.BindTexture(gl.TEXTURE_2D, 0)
	}
}

func drawQuadTex(x, y, w, h int, c Color, tex uint32) {
	_drawQuad(x, y, w, h, c, true, tex, false)
}

func drawQuadTexFlipY(x, y, w, h int, c Color, tex uint32) {
	_drawQuad(x, y, w, h, c, true, tex, true)
}
