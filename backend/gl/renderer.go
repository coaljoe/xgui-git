package glbackend

import (
	"fmt"
	"github.com/go-gl/gl/v2.1/gl"
	. "bitbucket.org/coaljoe/xgui/types"
	//. "bitbucket.org/coaljoe/rx/math"
)

type GlRenderer struct {
	drawColor Color
	width, height int
	//resX, resY int
	//prevX, prevY int
	//br        *sdl.Renderer
}

func NewGlRenderer() *GlRenderer {
	r := &GlRenderer{
		drawColor: Color{255, 255, 255, 255},
	}
	return r
}

/*
// Get backend's hardware renderer
func (r *GlRenderer) Renderer() *sdl.Renderer {
	//return r.br
	return nil
}
*/

func (r *GlRenderer) Init() { //renderer *sdl.Renderer) {
	println("glrenderer init")
	//r.br = renderer
	//r.width = width
	//r.height = height
	
	var glVp [4]int32
	gl.GetIntegerv(gl.VIEWPORT, &glVp[0])

	w := int(glVp[2])
	h :=  int(glVp[3])
	
	r.width = w
	r.height = h
	
	println("done glrenderer init")
}

func (r *GlRenderer) Size() (int, int) {
	return r.width, r.height
}

func (r *GlRenderer) HasClipRect() bool {
	panic("not implemented")
}

func (r *GlRenderer) SetClipRect(x, y, w, h int) {
	panic("not implemented")
}

func (r *GlRenderer) UnsetClipRect() {
	panic("not implemented")
}

// XXX Pre/Post -render?
func (r *GlRenderer) Set2dMode() {}
func (r *GlRenderer) Unset2dMode() {}

func (r *GlRenderer) PreRender() {

	var _ = `
	//resx := 800
	//resy := 600

	var glVp [4]int32
	gl.GetIntegerv(gl.VIEWPORT, &glVp[0])

	w := int(glVp[2])
	h :=  int(glVp[3])

	//println(w, h)
	//panic(2)

	//r.prevX = w
	//r.prevY = h
	r.resX = w
	r.resY = h
	`
	
	w := r.width
	h := r.height

	// Prepare
	//gl.PushAttrib(gl.CURRENT_BIT | gl.TEXTURE_BIT | gl.LIGHTING_BIT | gl.DEPTH_TEST)
	gl.PushAttrib(gl.CURRENT_BIT | gl.TEXTURE_BIT | gl.DEPTH_TEST)
		gl.Disable(gl.DEPTH_TEST)
		//gl.Disable(gl.LIGHTING)
	gl.Color3f(1, 1, 1) // important

	// Set 2d mode
	gl.MatrixMode(gl.PROJECTION)
	gl.PushMatrix()
	gl.LoadIdentity()
	gl.Viewport(0, 0, int32(w), int32(h))
	gl.MatrixMode(gl.MODELVIEW)
	gl.PushMatrix()
	gl.LoadIdentity()
	//glOrtho(0, screenWidth, 0, screenHeight, 1, -1); // Origin in lower-left corner
	gl.Ortho(0, float64(w), float64(h), 0, 1, -1) // Origin in upper-left corner
}

func (r *GlRenderer) PostRender() {
	// Restore
	gl.MatrixMode(gl.PROJECTION)
	gl.PopMatrix()
	gl.MatrixMode(gl.MODELVIEW)
	gl.PopMatrix()

	gl.PopAttrib()
}

func (r *GlRenderer) SetDrawColor(r_, g, b, a int) {
	//r.drawColor = sdl.Color{R: uint8(r_), G: uint8(g), B: uint8(g), A: uint8(a)}
	//r.br.SetDrawColor(uint8(r_), uint8(g), uint8(b), uint8(a))
	r.drawColor = Color{r_, g, b, a}

	cv := r.drawColor.ToVec4()
	//gl.Color4f(float32(cv[0]), float32(cv[1]), float32(cv[2]), float32(cv[3]))
	gl.Color3f(float32(cv[0]), float32(cv[1]), float32(cv[2]))
}

func (r *GlRenderer) DrawLine(x1, y1, x2, y2 int) {
	println("GlRenderer.DrawLine")
	println(x1, y1, x2, y2)
	//r.br.DrawLine(int32(x1), int32(y1), int32(x2), int32(y2))
	//panic(2)

	//gl.Color3f(1, 0, 0)
	gl.Begin(gl.LINES)
	gl.Vertex2i(int32(x1), int32(y1))
	gl.Vertex2i(int32(x2), int32(y2))
	gl.End()
}

func (r *GlRenderer) DrawRect(x, y, w, h int, c Color) {
	println("GlRenderer.DrawRect")
	fmt.Println(x, y, w, h, c)
	//panic(4)
	

	//gl.Color3f(1, 0, 0)
	gl.Color3f(0, 1, 0)
	gl.Begin(gl.LINES)
	gl.Vertex2f(0, 0)
	//gl.Vertex2f(1000, 1000)
	gl.Vertex2f(100, 100)
	gl.End()

	cv := c.ToVec4()
	gl.Color3f(float32(cv[0]), float32(cv[1]), float32(cv[2]))

	// CCW
       gl.Begin(gl.QUADS)
       gl.Vertex2i(int32(x), int32(y)) // vertex 1
       gl.Vertex2i(int32(x+w), int32(y)) // vertex 2
       gl.Vertex2i(int32(x+w), int32(y+h)) // vertex 3
       gl.Vertex2i(int32(x), int32(y+h)) // vertex 4
       gl.End()

	return
	
	/*
	err := r.br.SetDrawBlendMode(sdl.BLENDMODE_BLEND)
	if err != nil {
		panic(err)
	}

	r.br.SetDrawColor(uint8(c.R()), uint8(c.G()), uint8(c.B()), uint8(c.A()))
	var rect sdl.Rect
	rect.X = int32(x)
	rect.Y = int32(y)
	rect.W = int32(w)
	rect.H = int32(h)
	r.br.FillRect(&rect)

	err = r.br.SetDrawBlendMode(sdl.BLENDMODE_NONE)
	if err != nil {
		panic(err)
	}
	*/
}
