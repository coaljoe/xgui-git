package sdlbackend

import (
	//"fmt"
	"github.com/veandco/go-sdl2/sdl"
	. "bitbucket.org/coaljoe/xgui/types"
	//ttf "github.com/veandco/go-sdl2/ttf"
	"bitbucket.org/coaljoe/xgui/backend"
	"strings"
)

// Sdl text rendering backend
type TextRenderer struct {
}

func NewTextRenderer() *TextRenderer {
	tr := &TextRenderer{
	}
	return tr
}

func (tr *TextRenderer) RenderTextColor(text string, x, y int, color Color, font backend.FontI) {
	tr.renderTextColor(text, x, y, color, font, nil)
}

// Creates a new image
func (tr *TextRenderer) RenderTextColorAsImage(text string, tw, th int, color Color, font backend.FontI) backend.ImageI {
	//println("RenderTextColorAsImage text:", text)
	
	im := NewImage()

	x := 0
	y := 0
	_ = x
	_ = y

	surf, err := sdl.CreateRGBSurface(0, int32(tw), int32(th), 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000)
	if err != nil {
		panic(err)
	}
	im.surf = surf

	//surf.Free()

	//return nil

	// Fill textures pixel values with text's color to avoid issues
	// with font anti-aliasing
	// https://forums.libsdl.org/viewtopic.php?p=51795
	//c := sdl.MapRGBA(surf.Format, uint8(color[0]), uint8(color[1]), uint8(color[2]), uint8(color[3]))
	c := sdl.MapRGBA(surf.Format, uint8(color[0]), uint8(color[1]), uint8(color[2]), 0)
	surf.FillRect(nil, c)
	
	tr.renderTextColor(text, x, y, color, font, surf)

	im.Build()
	//im.Build2()

	//im.surf = nil
	//surf.Free()

	//return nil

	// XXX cleanup
	surf.Free()
	im.surf = nil
	
	//fmt.Printf("%#v\n", im)
	//panic(2)
	return im
}


func (tr *TextRenderer) renderTextColor(text string, x, y int, color Color, font backend.FontI, targetSurf *sdl.Surface) {
	//tr.renderLine(text, x, y, color, font)
	//return

	lines := strings.Split(text, "\n")
	ysh := 0
	
	// XXX calculate height of arbitrary string, fixme?
	_, yshInc := tr.sizeLine("1", font)

	for _, line := range lines {
		if targetSurf == nil {
			tr.renderLine(line, x, y + ysh, color, font, nil)
		} else {
			tr.renderLine(line, x, y + ysh, color, font, targetSurf)
		}
		//ysh += font.Size()
		//ysh += 20
		ysh += yshInc
	} 
}

func (tr *TextRenderer) renderLine(text string, x, y int, color Color, font backend.FontI, targetSurf *sdl.Surface) {
	if text == "" {
		return
	}

	r := sbi.r.br

	var color_ sdl.Color
	/*
		color_.R = uint8(color[0] * 255)
		color_.G = uint8(color[1] * 255)
		color_.B = uint8(color[2] * 255)
		color_.A = uint8(color[3] * 255)
	*/
	color_.R = uint8(color[0])
	color_.G = uint8(color[1])
	color_.B = uint8(color[2])
	color_.A = uint8(color[3])

	// XXX hacky, fixme?
	sdlFont := font.(*Font).sdlFont

	//surf, err := ctx.Style.textFont.RenderUTF8Solid(text, color_)
	//surf, err := tr.font.RenderUTF8Blended(text, color_)
	var surf *sdl.Surface
	var err error
	if font.AA() {
		surf, err = sdlFont.RenderUTF8Blended(text, color_)
	} else {
		surf, err = sdlFont.RenderUTF8Solid(text, color_)
	}
	if err != nil {
		panic(err)
	}

	if targetSurf != nil {
		// XXX can be used with a render target
		//r.SetDrawColor(color_.R, color_.G, color_.B, color_.A)
		//r.Clear()
		
		var rect sdl.Rect
		rect.X = int32(x)
		rect.Y = int32(y)
		rect.W = surf.W
		rect.H = surf.H
		surf.Blit(nil, targetSurf, &rect)
		//*targetSurf = *surf
	} else {

		tex, err2 := r.CreateTextureFromSurface(surf)
		if err2 != nil {
			panic(err2)
		}

		var rect sdl.Rect
		rect.X = int32(x)
		rect.Y = int32(y)
		rect.W = surf.W
		rect.H = surf.H

		r.Copy(tex, nil, &rect)

		err = tex.Destroy()
		if err != nil {
			panic(err)
		}
	}

	// Cleanup
	surf.Free()
}

func (tr *TextRenderer) SizeText(text string, font backend.FontI) (int, int) {
	//return tr.sizeLine(text, font)

	//s := strings.TrimRight(text, "\n")
	s := text
	//println("XXX s:", s)
	lines := strings.Split(s, "\n")
	//println("len:", len(lines))
	//println("0 ", lines[0])
	//println("1 ", lines[1])

	// XXX hacky, fixme?
	sdlFont := font.(*Font).sdlFont
	// Recommended line height
	lineH := sdlFont.LineSkip()
	
	sx, sy := 0, 0
	for i, line := range lines {
		_ = i
		//println("derp")
		//println("->", i, line)
		w, h := tr.sizeLine(line, font)

		if w > sx {
			sx = w
		}
		
		_ = h
		//sy += 10
		//sy += h
		sy += lineH
		//sy += 20 // font.Size()
	}

	return sx, sy
}

func (tr *TextRenderer) sizeLine(text string, font backend.FontI) (int, int) {
	// XXX hacky, fixme?
	sdlFont := font.(*Font).sdlFont

	//w, h, err := tr.font.SizeUTF8(text)
	w, h, err := sdlFont.SizeUTF8(text)
	if err != nil {
		panic(err)
	}
	return w, h
}

var _ = `
func (tr *TextRenderer) RenderTextColor(text string, x, y float64, color Color) {

	sx := 2.0 / float64(rxi.Renderer().Width())
	sy := 2.0 / float64(rxi.Renderer().Height())

	x = -1.0 + (x * 2)
	y = (y * 2) - 1.0

	//p(tr.font.ft_font.MaxAdvanceHeight)
	//pp(tr.font.ft_font.BBox)
	//yAdj := float64(tr.font.ft_font.MaxAdvanceHeight/64) * rxi.Renderer().GetPh()
	//yAdj := float64(tr.font.ft_font.Ascender/64) * sy
	yAdj := float64(tr.font.Height()*2) * rxi.Renderer().GetPh()
	//yAdj := float64(3442/64) * rxi.Renderer().GetPh()
	y += yAdj
	//pp(float64(tr.font.ft_font.LineHeight/64) * rxi.Renderer().GetPh())

	// FIXME: text rendering doesn't work with culling enabled
	// disable culling for whole GUI pass?
	prevCullEnabled := false
	if gl.IsEnabled(gl.CULL_FACE) {
		prevCullEnabled = true
		gl.Disable(gl.CULL_FACE)
	}

	prevBlendEnabled := false
	if gl.IsEnabled(gl.BLEND) {
		prevBlendEnabled = true
	} else {
		gl.Enable(gl.BLEND)
		gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	}
	gl.BindTexture(gl.TEXTURE_2D, tr.gl_tex)

	tr.shader.Bind()
	tr.shader.PassUniform1i("tex", 0)
	tr.shader.PassUniform4f_("color", color.ToVec4())

	gl.EnableVertexAttribArray(uint32(tr.gl_attribute_coord))
	gl.BindBuffer(gl.ARRAY_BUFFER, tr.gl_vbo)
	gl.VertexAttribPointer(uint32(tr.gl_attribute_coord), 4, gl.FLOAT,
		false, 0, gl.PtrOffset(0))

	for runeIdx, rune := range text {
		//c := text[i]
		//fmt.Printf("rune: %#U\n", rune)

		skipRendering := false
		if unicode.IsSpace(rune) {
			skipRendering = true
		}

		font := tr.font.ft_font
		gIndex := font.Index(rune)
		//p("index:", gIndex)
		g, err := font.Load(gIndex)
		if err != nil {
			panic(fmt.Sprintf(
				"Failed to load glyph: %#U at byte position %d",
				rune, runeIdx))
		}
		gImage, err := g.Image()
		if err != nil {
			panic(err)
		}
		_ = gImage

		// Size in pixels
		gWidth := g.Width / 64
		gHeight := g.Height / 64

		//assert(gImage.ColorModel() == _color.AlphaModel)
		//p(len(gImage.Pix), g.Width, g.Height, gImage.Bounds())
		//pp(2)

		if false && !unicode.IsSpace(rune) {
			outFile, err := os.Create("/tmp/rxgui_test_freetype_out.png")
			if err != nil {
				panic(err)
			}

			err = png.Encode(outFile, gImage)
			if err != nil {
				panic(err)
			}
			println("wrote /tmp/rxgui_test_freetype_out.png file")
		}

		if !skipRendering {
			// Upload the glyph's image
			gl.TexImage2D(gl.TEXTURE_2D, 0, gl.ALPHA,
				int32(gWidth), int32(gHeight),
				0, gl.ALPHA, gl.UNSIGNED_BYTE, gl.Ptr(gImage.Pix))
		}

		// Calculate vertex and texture coord
		bitmap_left := float32(g.HMetrics.BearingX / 64)
		//bitmap_top := float32(-g.VMetrics.BearingY / 64)
		bitmap_top := float32(g.VMetrics.BearingY / 64)
		bitmap_width := float32(gWidth)
		bitmap_rows := float32(gHeight)

		x2 := float32(x) + bitmap_left*float32(sx)
		y2 := float32(-y) + bitmap_top*float32(sy)
		w := bitmap_width * float32(sx)
		h := bitmap_rows * float32(sy)

		// x, y, s, t
		box := []float32{
			x2, -y2, 0, 0,
			x2 + w, -y2, 1, 0,
			x2, -y2 - h, 0, 1,
			x2 + w, -y2 - h, 1, 1,
		}
		//p(box)

		// Draw the character on the screen
		gl.BufferData(gl.ARRAY_BUFFER, 16*4, gl.Ptr(&box[0]), gl.DYNAMIC_DRAW)
		//gl.BufferData(gl.ARRAY_BUFFER, 16*4, gl.Ptr(box), gl.DYNAMIC_DRAW)
		gl.DrawArrays(gl.TRIANGLE_STRIP, 0, 4)

		// Advance the cursor to the start of the next character
		advance_x := g.HMetrics.Advance / 64
		//pp(advance_x, float64(advance_x)*sx)
		//advance_y := g.VMetrics.Advance / 64
		//p(advance_y, float64(advance_y)*sy)
		//x += float64(advance_x>>6) * sx
		//y += float64(advance_y>>6) * sy
		//x += 0.05
		x += float64(advance_x) * sx
		//y += float64(advance_y) * sy
		y += 0
	}

	gl.DisableVertexAttribArray(uint32(tr.gl_attribute_coord))
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindTexture(gl.TEXTURE_2D, 0)
	tr.shader.Unbind()

	if !prevBlendEnabled {
		gl.Disable(gl.BLEND)
	}

	if prevCullEnabled {
		gl.Enable(gl.CULL_FACE) // Restore culling
	}

	rx.Xglcheck()
}
`
