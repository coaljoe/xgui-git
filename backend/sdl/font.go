package sdlbackend

import (
	"fmt"
	"os"

	//"github.com/veandco/go-sdl2/sdl"

	ttf "github.com/veandco/go-sdl2/ttf"
)

type Font struct {
	path string
	size int
	loaded bool
	aa bool
	sdlFont *ttf.Font
}

func NewFont() *Font {
	f := &Font{
		aa: true,	
	}
	return f
}

func (f *Font) Load(path string, size int) {
	var font *ttf.Font
	var err error
	//sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
	//if font, err = ttf.OpenFont("res/gui/fonts/DejaVuSans.ttf", 16); err != nil {
	if font, err = ttf.OpenFont(path, size); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open font: %s\n", err)
		panic(2)
	}

	/*
	//font.SetHinting(ttf.HINTING_NONE)
	font.SetHinting(ttf.HINTING_NORMAL)

	h := font.GetHinting()
	fmt.Println(h)
	//panic(2)

	font.SetStyle(ttf.STYLE_BOLD)
	
	s := font.GetStyle()
	fmt.Println(s)
	//panic(2)
	*/
	
	f.sdlFont = font
	f.size = size
	f.path = path
	f.loaded = true
}

func (f *Font) AA() bool {
	return f.aa
}

func (f *Font) SetAA(v bool) {
	f.aa = v	
}