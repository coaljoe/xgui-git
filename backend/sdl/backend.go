package sdlbackend

import (
	//ps "bitbucket.org/coaljoe/lib/pubsub"
	//"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"bitbucket.org/coaljoe/xgui/backend"
	//"os"
)

// Globals
var sbi *SdlBackend

type SdlBackend struct {
	r              *SdlRenderer
	appWindowTitle string
	appWindow      *sdl.Window
	tr *TextRenderer
	xgi backend.XGuiInstanceI
}

func NewSdlBackend() *SdlBackend {
	sbi = &SdlBackend{
		appWindowTitle: "SdlBackend Window",
	}
	return sbi
}

// FIXME: temporary placeholder(?)
func (sb *SdlBackend) Renderer() backend.RendererI {
	return sb.r
}

func (sb *SdlBackend) TextRenderer() backend.TextRendererI {
	return sb.tr
}

func (sb *SdlBackend) Register(xgi backend.XGuiInstanceI) {
	sb.xgi = xgi
}

func (sb *SdlBackend) Init(renderer *sdl.Renderer) {
	println("sdlbackend init")

	sb.r = NewSdlRenderer()
	sb.r.Init(renderer)

	sb.tr = NewTextRenderer()

	println("done sdlbackend init")
}

func (sb *SdlBackend) NewImage() backend.ImageI {
	return NewImage()
}

func (sb *SdlBackend) NewFont() backend.FontI {
	return NewFont()
}
