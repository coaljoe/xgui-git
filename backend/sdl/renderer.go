package sdlbackend

import (
	//ps "bitbucket.org/coaljoe/lib/pubsub"
	//"fmt"
	"github.com/veandco/go-sdl2/sdl"
	. "bitbucket.org/coaljoe/xgui/types"
)

type SdlRenderer struct {
	drawColor sdl.Color
	clipRect *sdl.Rect
	br        *sdl.Renderer
}

func NewSdlRenderer() *SdlRenderer {
	r := &SdlRenderer{
		drawColor: sdl.Color{R: 255, G: 255, B: 255, A: 255},
	}
	return r
}

// Get backend's hardware renderer
func (r *SdlRenderer) Renderer() *sdl.Renderer {
	return r.br
}

func (r *SdlRenderer) Init(renderer *sdl.Renderer) {
	println("sdlrenderer init")
	r.br = renderer
	println("done sdlrenderer init")
}

func (r *SdlRenderer) Size() (int, int) {
	w, h, err := r.br.GetOutputSize()
	if err != nil {
		panic(err)
	}
	resX := int(w)
	resY := int(h)
	return resX, resY
}

func (r *SdlRenderer) SetDrawColor(r_, g, b, a int) {
	r.drawColor = sdl.Color{R: uint8(r_), G: uint8(g), B: uint8(g), A: uint8(a)}
	r.br.SetDrawColor(uint8(r_), uint8(g), uint8(b), uint8(a))
}

func (r *SdlRenderer) DrawLine(x1, y1, x2, y2 int) {
	//println("SdlRenderer.DrawLine")
	r.br.DrawLine(int32(x1), int32(y1), int32(x2), int32(y2))
}

func (r *SdlRenderer) DrawRect(x, y, w, h int, c Color) {
	//println("SdlRenderer.DrawRect")
	err := r.br.SetDrawBlendMode(sdl.BLENDMODE_BLEND)
	if err != nil {
		panic(err)
	}

	r.br.SetDrawColor(uint8(c.R()), uint8(c.G()), uint8(c.B()), uint8(c.A()))
	var rect sdl.Rect
	rect.X = int32(x)
	rect.Y = int32(y)
	rect.W = int32(w)
	rect.H = int32(h)
	r.br.FillRect(&rect)

	err = r.br.SetDrawBlendMode(sdl.BLENDMODE_NONE)
	if err != nil {
		panic(err)
	}
}

func (r *SdlRenderer) SetClipRect(x, y, w, h int) {
	var clipRect sdl.Rect
	clipRect = sdl.Rect{int32(x), int32(y),
		int32(w), int32(h)}

	r.br.SetClipRect(&clipRect)

	r.clipRect = &clipRect
}

func (r *SdlRenderer) UnsetClipRect() {
	r.br.SetClipRect(nil)

	r.clipRect = nil
}

func (r *SdlRenderer) HasClipRect() bool {
	return r.clipRect != nil	
}


func (r *SdlRenderer) PreRender() {}
func (r *SdlRenderer) PostRender() {}

