package sdlbackend

import (
	//ps "bitbucket.org/coaljoe/lib/pubsub"
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	//xg "bitbucket.org/coaljoe/xgui"
	"bitbucket.org/coaljoe/xgui/backend"
	"os"
)

type SdlApp struct {
	//*xg.App // BaseApp
	*backend.BaseApp
	title string
	window     *sdl.Window
	//renderer *sdl.Renderer
	sb *SdlBackend

}

func NewSdlApp() *SdlApp {
	a := &SdlApp{
		BaseApp: backend.NewBaseApp(),
		title: "SdlBackend Window",
	}
	return a
}

func (a *SdlApp) Renderer() *sdl.Renderer {
	//return a.renderer
	return a.sb.r.br
}

func (a *SdlApp) Backend() *SdlBackend {
	return a.sb
}



func (a *SdlApp) Init(width, height int, fullscreen bool) {
	println("sdlapp init")
	a.BaseApp.Init(width, height, fullscreen)

	title := a.title

	var err error
	if !fullscreen {
		a.window, err = sdl.CreateWindow(title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
			int32(width), int32(height), sdl.WINDOW_SHOWN|sdl.WINDOW_OPENGL)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
			panic(1)
		}
	} else {
		flags := uint32(sdl.WINDOW_SHOWN | sdl.WINDOW_OPENGL)
		flags |= sdl.WINDOW_FULLSCREEN
		a.window, err = sdl.CreateWindow(title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
			int32(width), int32(height), flags)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
			panic(1)
		}
	}

	renderer, err2 := sdl.CreateRenderer(a.window, -1, sdl.RENDERER_ACCELERATED|sdl.RENDERER_PRESENTVSYNC)
	//renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err2)
		panic(2)
	}
	//defer renderer.Destroy()

	// XXX set the default to 0 explicitly (seems not presented on all sdl versions)
	sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "0")

	sb := NewSdlBackend()
	sb.Init(renderer)
	a.sb = sb

	//a.r = NewSdlRenderer()
	//a.r.Init(renderer)
	println("done sdlapp init")
}

func (a *SdlApp) ProcessEvents() {

	var event sdl.Event
	//event := sdl.WaitEvent()
	for event = sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			//a.running = false
			a.SetRunning(false)
		//case *sdl.KeyUpEvent:
		//	fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n",
		//		t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)

		case *sdl.KeyboardEvent:
			fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n",
				t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)
			if t.Keysym.Sym == 'q' {
				//a.running = false
				a.SetRunning(false)
			}

		case *sdl.MouseMotionEvent:
			fmt.Printf("[%d ms] MouseMotion\tid:%d\tx:%d\ty:%d\txrel:%d\tyrel:%d\n", t.Timestamp, t.Which, t.X, t.Y, t.XRel, t.YRel)
			
			a.sb.xgi.InjectMouseMove(int(t.X), int(t.Y))
			//panic(2)
			var _ = `
			vars.mx, vars.my = int(t.X), int(t.Y)
			prevX := int(t.X - t.XRel)
			prevY := int(t.Y - t.YRel)
			sX := float64(t.X) / float64(vars.resX)
			sY := float64(t.Y) / float64(vars.resY)
			evData := xg.EvMouseMoveData{int(t.X), int(t.Y), prevX, prevY, sX, sY,
				float64(prevX) / float64(vars.resX), float64(prevY) / float64(vars.resY)}
			_ = evData
			
			if game.gui.enabled {
				
					p(evData)
					//xg.Pub(xg.Ev_gui_mouse_move, evData)
					game.gui.xgi.Context.InputSys.OnMouseMove(evData)
					if game.hud.enabled {
						game.hud.onMouseMove(evData)
					}
                    
			}
            `

		case *sdl.MouseButtonEvent:
			
			//pub(ev_mouse_button_event, t)
			if t.State == sdl.PRESSED {
				btn := int(t.Button)
				a.sb.xgi.InjectMouseButton(btn, true)
				var _ = `
					if game.gui.enabled {
						game.gui.xgi.Context.InputSys.OnMouseButton(t.Button, true)
						if game.hud.enabled {
							game.hud.onMouseButton(t.Button, true)
						}
					}
                    `
			} else if t.State == sdl.RELEASED {
				btn := int(t.Button)
				a.sb.xgi.InjectMouseButton(btn, false)
				var _ = `
					if game.gui.enabled {
						game.gui.xgi.Context.InputSys.OnMouseButton(t.Button, false)
						if game.hud.enabled {
							game.hud.onMouseButton(t.Button, false)
						}
					}
                    `
			}

			//panic(2)

		}
	}

	//println("main.step")
	/*
		renderer.Clear()
		renderer.SetDrawColor(255, 0, 0, 255)
		renderer.FillRect(&sdl.Rect{0, 0, int32(winWidth), int32(winHeight)})
		//renderer.Copy(texture, &src, &dst)
		renderer.Present()
	*/

}

func (a *SdlApp) Draw() {
	println("sdlapp draw")
	renderer := a.Renderer()

	// Reset Color
	renderer.SetDrawColor(0, 0, 0, 255)
	// Clear
	renderer.Clear()

	// Draw gui here
	//xgi.Render(renderer)

	fmt.Printf("xgi: %#v\n", a.sb.xgi)

	a.sb.xgi.Render()

	renderer.Present()
}

func (a *SdlApp) Step() bool {
	println("sdlapp step")
	if !a.Running() {
		a.Quit()
		return false
	}

	a.PreStep()

	a.ProcessEvents()


	a.Update(a.Dt())
	a.Draw()

	a.PostStep()

	return true
}

func (a *SdlApp) Quit() {
	println("App.Quit")
	a.SetRunning(false)
	sdl.Quit()
}

func (a *SdlApp) Update(dt float64) {
	a.sb.xgi.Update(dt)
}
