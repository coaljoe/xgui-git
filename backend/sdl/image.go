package sdlbackend

import (
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	//"unsafe"
	"math"
		. "bitbucket.org/coaljoe/xgui/types"	
)

type Image struct {
	surf *sdl.Surface
	tex  *sdl.Texture
	w, h  int
	//Scale bool
	// XXX should be set before Create(!)
	scaleFilter bool
	prevSQVal   string
}

func NewImage() *Image {
	i := &Image{
		tex:         nil,
		scaleFilter: false,
	}
	return i
}

func (i *Image) Surf() *sdl.Surface { return i.surf }
func (i *Image) ScaleFilter() bool { return i.scaleFilter }
func (i *Image) SetScaleFilter(v bool) { i.scaleFilter = v }
func (i *Image) W() int { return i.w }
func (i *Image) H() int { return i.h }

// XXX for debug only
func (i *Image) TexW() int {
	_, _, w, _, err := i.tex.Query()
	if err != nil {
		panic(err)
	}

	return int(w)
}

// XXX for debug only
func (i *Image) TexH() int {
	_, _, _, h, err := i.tex.Query()
	if err != nil {
		panic(err)
	}

	return int(h)
}

func (i *Image) Load(path string) {

	//ret := sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
	//_ = ret

	i.pushScaleFilter()

	surf, err := img.Load(path)
	if err != nil {
		panic(err)
	}
	i.surf = surf

	/*
		i.tex, err = ctx.Renderer.CreateTextureFromSurface(i.surf)
		if err != nil {
			panic(err)
		}
	*/

	i.Build()

	i.popScaleFilter()
}

/*
func (i *Image) Build2() {
	var err error
	i.tex, err = sbi.r.br.CreateTextureFromSurface(i.surf)
	if err != nil {
		panic(err)
	}

	//if i.surf != nil {
	//i.surf.Free()
	//i.surf = nil
	//}

	i.w = int(i.surf.W) //i.TexW()
	i.h = int(i.surf.H) //i.TexH()

	//i.tex.Destroy()
}
*/

func (i *Image) Build() {
	var err error
	i.tex, err = sbi.r.br.CreateTextureFromSurface(i.surf)
	if err != nil {
		panic(err)
	}

	//i.surf = nil

	i.w = int(i.surf.W) //i.TexW()
	i.h = int(i.surf.H) //i.TexH()
}

func (i *Image) Create(w, h int) {
	//ret := sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
	//_ = ret

	i.pushScaleFilter()

	surf, err := sdl.CreateRGBSurface(0, int32(w), int32(h), 32, 0, 0, 0, 0)
	if err != nil {
		panic(err)
	}
	i.surf = surf

	i.Build()

	/*
		//var err error
		i.tex, err = ctx.Renderer.CreateTexture(sdl.PIXELFORMAT_RGB888, sdl.TEXTUREACCESS_STATIC, int32(w), int32(h))
		if err != nil {
			panic(err)
		}
	*/

	i.popScaleFilter()
}

/*
func (i *Image) PutPixel(x, y, r, g, b int) {
	format, _, w, h, err := i.tex.Query()
	if err != nil {
		panic(err)
	}
	//color := sdl.MapRGB(&format, uint8(r), uint8(g), uint8(b))
	xf := sdl.PIXELFORMAT_ABGR8888
	color := sdl.MapRGB(&xf, uint8(r), uint8(g), uint8(b))
	pixels, pitch, err := i.tex.Lock(nil)
	if err != nil {
		panic(err)
	}
	var u uint
	uintSize := int(unsafe.Sizeof(u))
	pp(uintSize)
	pixelPosition := y*(pitch/uintSize) + x
	pixels[pixelPosition] = color
	i.tex.Unlock()
	//pixels := i.tex.Query()
}
*/

func (i *Image) pushScaleFilter() {
	if i.scaleFilter {
		// Push
		i.prevSQVal = sdl.GetHint(sdl.HINT_RENDER_SCALE_QUALITY)

		// XXX FIXME will override previous settings!

		//SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
		//ret := sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
		ret := sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
		_ = ret
		//pp(ret)
	}
}

// XXX should be used after Build()?
func (i *Image) popScaleFilter() {
	if i.scaleFilter {
		// Pop
		ret := sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, i.prevSQVal)
		_ = ret
	}
}

func (i *Image) PutPixel(x, y, r, g, b int, update bool) {
	color := sdl.MapRGB(i.surf.Format, uint8(r), uint8(g), uint8(b))
	rect := sdl.Rect{int32(x), int32(y), 1, 1}
	i.surf.FillRect(&rect, color)
	if update {
		i.Build()
	}
}

func (i *Image) HasImage() bool {
	return i.tex != nil
}

func (i *Image) Free() {
	if i.surf != nil {
		i.surf.Free()
		i.surf = nil
	}

	if i.tex != nil {
		i.tex.Destroy()
		i.tex = nil
	}
}

func (i *Image) Render(x, y, w, h int) {
	r := sbi.r.br

	xw, xh := int32(w), int32(h)
	if w == -1 && h == -1 {
		/*
		var err error
		_, _, xw, xh, err = i.tex.Query()
		if err != nil {
			panic(err)
		}
		*/
		xw, xh = int32(i.w), int32(i.h)
	}
	dstrect := sdl.Rect{int32(x), int32(y), xw, xh}
	//pp(dstrect)
	/*
		if i.Scale {
			r.SetScale(0.5, 0.5)
		}
	*/
	r.Copy(i.tex, nil, &dstrect)
	/*
		if i.Scale {
			r.SetScale(1.0, 1.0)
		}
	*/

}

func (i *Image) RenderScale(x, y, w, h int, sx, sy float64) {
	r := sbi.r.br

	curSx, curSy := r.GetScale()
	r.SetScale(float32(sx), float32(sy))
	//xs := int(float64(x) / sx)
	//ys := int(float64(y) / sy)
	xs := int(math.Ceil(float64(x) / sx))
	ys := int(math.Ceil(float64(y) / sy))
	//pp(x, xs, sx)
	i.Render(xs, ys, w, h)
	r.SetScale(curSx, curSy)
}

func (i *Image) RenderRect(srcrect Rect, dstrect Rect) {
	r := sbi.r.br
	convSrcRect := sdl.Rect{int32(srcrect.X()), int32(srcrect.Y()), int32(srcrect.W()), int32(srcrect.H())}
	convDstRect := sdl.Rect{int32(dstrect.X()), int32(dstrect.Y()), int32(dstrect.W()), int32(dstrect.H())}
	r.Copy(i.tex, &convSrcRect, &convDstRect)
}
