package backend

import (
	//	"fmt"
	//	"github.com/veandco/go-sdl2/sdl"
	"time"
)

//var renderer *sdl.Renderer

// BaseApp
type BaseApp struct {
	width      int
	height     int
	fullscreen bool
	title      string
	running    bool
	frame      int
	thisTime   time.Time
	lastTime   time.Time
	dt         float64
}

func (a *BaseApp) Running() bool     { return a.running }
func (a *BaseApp) SetRunning(v bool) { a.running = v }

func (a *BaseApp) Dt() float64 {
	return a.dt
}

func NewBaseApp() *BaseApp {
	a := &BaseApp{
		title: "xgui",
	}
	return a
}

func (a *BaseApp) Init(width, height int, fullscreen bool) {
	// check args
	if width < 1 || height < 1 {
		panic("bad width and height")
	}

	a.width = width
	a.height = height
	a.fullscreen = fullscreen

	a.running = true
}

/*
func (a *App) Draw() {
	// Reset Color
	renderer.SetDrawColor(0, 0, 0, 255)
	// Clear
	renderer.Clear()

	// Draw gui here
	xgi.Render(renderer)

	renderer.Present()
}
*/

func (a *BaseApp) PreStep() bool {
	if !a.running {
		//a.Quit()
		return false
	}

	thisTime := time.Now()
	passedTime := thisTime.Sub(a.lastTime)
	a.dt = passedTime.Seconds()
	a.thisTime = thisTime

	//ctx.Backend.ProcessEvents()

	return true
}

func (a *BaseApp) PostStep() {
	//a.Update(a.dt)
	//a.Draw()

	a.lastTime = a.thisTime
	a.frame += 1
}

/*
func (a *BaseApp) Quit() {
	println("App.Quit")
	a.running = false
	//sdl.Quit()
}
*/

/*
func (a *App) Update(dt float64) {

}
*/
