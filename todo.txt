todo:
- ? Patch9-less borders rendering (from style) for panels/buttons/etc
- ? style links test
- add pprof memprofile env handler ~ XGUI_MEMPPROF=1

unsort:
- ? debug module/system
- ? line height settings for font
- ? per-widget renderables like Card
- ? text antialiasing settings per-label or per-text
- ? use lighter alternative for image in text/label rendering, something like BackendTexture or Card
- ? move res/gui/base -> res/gui/xgui (or res/gui/xgui/base)

bugs:
- Panel.OnMouseEnterEv is not working