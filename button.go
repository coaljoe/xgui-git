package xgui

import (
	ps "bitbucket.org/coaljoe/lib/pubsub"
	. "bitbucket.org/coaljoe/xgui/types"
	//"fmt"
	"github.com/veandco/go-sdl2/sdl"
	//"bitbucket.org/coaljoe/xgui/backend"
)

type ButtonStyle struct {
	*Style
	NoPressed bool
}

func NewButtonStyle() *ButtonStyle {
	styleCopy := *ctx.Style
	s := &ButtonStyle{
		Style: &styleCopy,
	}
	return s
}

// A Widget.
type Button struct {
	*Widget
	Style *ButtonStyle
	// Properties
	Pressed bool
	// Components
	Label *Label
	Panel *Panel
	Icon  ImageI
	//StyleNoPressed bool
	//ButtonStyle *ButtonStyle
	prevLabelText string // XXX fixme: use events?
	dirty         bool
}

func NewButton(rect Rect) *Button {
	w := &Button{
		Widget: NewWidget("button"),
		//color:  &ColorWhite,
		//label:  NewLabel(Pos{0.5, 0.5}, "label"),
		Label: NewLabel(Pos{0, 0}, "label"),
	}
	w.Style = NewButtonStyle()
	w.BaseStyle = w.Style.Style

	w.rect = rect
	w.Label.SetText("ButtonLabelText")
	//w.label.SetText("x")
	w.Label.SetName("button_label")
	w.Label.border = false
	//w.label.TextAlign = TextAlignType_Center
	//w.label.SetPos(Pos{rect.W() / 2, rect.H() / 2})
	//w.label.SetPos(Pos{rect.W() / 2 - w.label.rect.W()/2, rect.H() / 2})
	w.Label.SetPassEvents(true)
	w.Label.MarkAsPrivate()
	//w.label.Style.OuterBorderColor = ColorRed
	//pp(w.rect, w.label.rect)

	//w.Panel = NewPanel(w.rect)
	//w.Panel = NewPanel(Rect{0, 0, 1, 1})
	//w.Panel = NewPanel(Rect{0, 0, 100, 100})
	//w.Panel = NewPanel(Rect{0, 0, 0, 0})
	w.Panel = NewPanel(Rect{0, 0, -1, -1})
	w.Panel.SetName("_button_panel")
	//w.Panel.SetRect(w.rect)
	w.Panel.Label.SetText("")
	//w.Panel.CustomPatch9 = ctx.Style.ButtonPatch9
	w.Panel.Style.PanelPatch9 = w.Style.ButtonPatch9
	w.Panel.SetPassEvents(true)
	//w.Panel.SetRect(w.rect)
	w.Panel.MarkAsPrivate()

	w.AddChild(w.Panel)
	w.AddChild(w.Label)

	w.dirty = true

	Sub(Ev_mouse_enter, w.OnMouseEnterEv)
	Sub(Ev_mouse_out, w.OnMouseOutEv)
	Sub(Ev_mouse_button, w.OnMouseButtonEv)
	return w
}

func (b *Button) OnMouseEnterEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != b.Id() {
		return
	}
	var _ = `
	if b.color != nil {
		//colorObj := Color(b.color.ToVec4().AddScalar(0.5))
		colorObj := Color{}.FromVec4(b.color.ToVec4().AddScalar(0.5))
		b.color = &colorObj
		b.color[3] = 1.0
	}
	//pp(12)
	`
}

func (b *Button) OnMouseOutEv(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != b.Id() {
		return
	}
	var _ = `
	if b.color != nil {
		//colorObj := Color(b.color.ToVec4().SubScalar(0.5))
		colorObj := Color{}.FromVec4(b.color.ToVec4().SubScalar(0.5))
		b.color = &colorObj
		b.color[3] = 1.0
		//pp(13)
	}
	`
}

func (b *Button) OnMouseButtonEv(ev *ps.Event) {
	//p("ev:")
	//dump(ev)
	//evData := ev.Data.(*EvMousePressData)
	evData := ev.Data.(*EvMouseButtonData)
	btn := evData.Button
	pressed := evData.Pressed

	if !pressed {
		//b.Panel.CustomPatch9 = ctx.Style.ButtonPatch9
		b.Panel.Style.PanelPatch9 = b.Style.ButtonPatch9
	}

	if evData.ActiveWidget == nil ||
		evData.ActiveWidget.Id() != b.Id() {
		return
	}

	b.Pressed = pressed

	if pressed && !b.Style.NoPressed {
		if btn == sdl.BUTTON_LEFT {
			//b.Panel.CustomPatch9 = ctx.Style.ButtonPressedPatch9
			b.Panel.Style.PanelPatch9 = b.Style.ButtonPressedPatch9
			//pp(2)
		}
	} else {
		//b.Panel.CustomPatch9 = ctx.Style.ButtonPatch9
		b.Panel.Style.PanelPatch9 = b.Style.ButtonPatch9
	}
}

func (b *Button) SetIcon(path string) {
	if b.Icon == nil {
		//b.Icon = backend.NewImage()
		b.Icon = ctx.Backend.NewImage()
	}
	b.Icon.SetScaleFilter(true)
	b.Icon.Create(5, 5)
	b.Icon.Load(path)
}

func (b *Button) UnsetIcon() {
	if b.Icon != nil {
		b.Icon = nil
	}
}

func (b *Button) build() {

	b.Label.SetPos(Pos{b.rect.W()/2 - b.Label.rect.W()/2,
		b.rect.H()/2 - b.Label.rect.H()/2})

	b.dirty = false
}

func (b *Button) Render() {
	r := ctx.Renderer

	//pp(2)
	/*
		r.SetDrawColor(255, 0, 0, 255)
		var rectangle sdl.Rect

		rectangle.X = 0
		rectangle.Y = 0
		rectangle.W = 50
		rectangle.H = 50
		r.FillRect(&rectangle)
	*/

	//pp(p.rect)

	/*
		//r.SetDrawColor(255, 0, 0, 255)
		r.SetDrawColor(uint8((*b.color).R() * 255), uint8((*b.color).G() * 255), uint8((*b.color).B() * 255), 255)
		var rectangle sdl.Rect

		rectangle.X = int32(UnitsToPxW(r, b.rect.X()))
		rectangle.Y = int32(UnitsToPxH(r, b.rect.Y()))
		rectangle.W = int32(UnitsToPxW(r, b.rect.W()))
		rectangle.H = int32(UnitsToPxH(r, b.rect.H()))
		r.FillRect(&rectangle)
	*/

	if !b.visible {
		return
	}

	b.Widget.Render()
	b.Widget.RenderChildren()

	//b.Panel.Render(r)

	//b.label.Render(r)

	if b.Icon != nil {
		absRect := b.GetAbsoluteRect()
		style := b.Style.ButtonPatch9
		styleBorderW := int(style.Rects[0].W())
		styleBorderH := int(style.Rects[1].H())

		x := ((absRect.X() + b.rect.W()/2) - (b.Icon.W() / 2)) + styleBorderW
		y := ((absRect.Y() + b.rect.H()/2) - (b.Icon.H() / 2)) + styleBorderH
		w := b.Icon.W() - styleBorderW*2
		h := b.Icon.H() - styleBorderH*2

		scale := false
		if scale {
			x = absRect.X() + styleBorderW
			y = absRect.Y() + styleBorderH
			w = b.rect.W() - styleBorderW*2
			h = b.rect.H() - styleBorderH*2
		}

		//pressDepth := styleBorderH
		pressDepth := 1
		if b.Pressed {
			var clipRect Rect
			if scale {
				clipRect = Rect{x, y, w, h}
			} else {
				// Size of button's internal area
				rx := absRect.X() + styleBorderW
				ry := absRect.Y() + styleBorderH
				rw := b.rect.W() - styleBorderW*2
				rh := b.rect.H() - styleBorderH*2
				clipRect = Rect{rx, ry, rw, rh}
				_ = clipRect
			}
			//r.SetClipRect(&clipRect)
			//r.SetClipRect(clipRect.X(), clipRect.Y(),
			//	clipRect.W(), clipRect.H())
			_ = r

			y += pressDepth
			x += pressDepth
			//h -= pressDepth
		}

		b.Icon.Render(x, y, w, h)

		if b.Pressed {
			//r.SetClipRect(nil)
			//r.UnsetClipRect()
		}
	}
}

func (b *Button) Update(dt float64) {
	b.Widget.Update(dt)
	//p.label.Update(dt)
	//b.label.SetPos(Pos{b.rect[0] + (b.rect[2] / 2), b.rect[1] + (b.rect[3] / 2)})
	/*
		absRect := b.GetAbsoluteRect()
		b.label.SetPos(Pos{absRect[0] + (absRect[2] / 2), absRect[1] + (absRect[3] / 2)})
	*/
	p("b.rect:", b.rect)
	//panic(2)

	if b.Label.text != b.prevLabelText {
		b.dirty = true
		b.prevLabelText = b.Label.text
	}

	if b.dirty {
		b.build()
	}
}
