module bitbucket.org/coaljoe/xgui

go 1.13

require (
	bitbucket.org/coaljoe/lib v0.0.0-20191227201005-c4231c006d95
	bitbucket.org/coaljoe/lib/debug v0.0.0-20191227201005-c4231c006d95
	bitbucket.org/coaljoe/rx/math v0.0.0-20191226054839-433c93a7e71f
	github.com/go-gl/gl v0.0.0-20181026044259-55b76b7df9d2
	github.com/go-gl/glfw v0.0.0-20181213070059-819e8ce5125f
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/veandco/go-sdl2 v0.4.1
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
)

//replace bitbucket.org/coaljoe/lib/xlog => ../lib/xlog
//replace bitbucket.org/coaljoe/lib/debug => ../lib/debug
