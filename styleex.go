package xgui

func (s *PanelStyle) Copy() *PanelStyle {
	stCopy := *s
	//stCopy.Style = *s.Style
	//stBaseCopy := s.CopyBaseStyle()
	//stCopy.Style = stBaseCopy
	stCopy.Style = s.CopyBaseStyle()
	return &stCopy
}

func (s *ButtonStyle) Copy() *ButtonStyle {
	stCopy := *s
	stCopy.Style = s.CopyBaseStyle()
	return &stCopy
}

func (s *ImageBoxStyle) Copy() *ImageBoxStyle {
	stCopy := *s
	stCopy.Style = s.CopyBaseStyle()
	return &stCopy
}

func (s *LabelStyle) Copy() *LabelStyle {
	stCopy := *s
	stCopy.Style = s.CopyBaseStyle()
	return &stCopy
}

func (s *SliderStyle) Copy() *SliderStyle {
	stCopy := *s
	stCopy.Style = s.CopyBaseStyle()
	return &stCopy
}

func (s *SpinButtonStyle) Copy() *SpinButtonStyle {
	stCopy := *s
	stCopy.Style = s.CopyBaseStyle()
	return &stCopy
}
