package xgui

//import (
//	"bitbucket.org/coaljoe/xgui/backend"
//)

/*
type BackendI interface {
	//InitWindow(width, height int, fullscreen bool)
	//ProcessEvents()
	Register()
}
*/

type RendererI interface {
	DrawRect(x, y, w, h int)
}
