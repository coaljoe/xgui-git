package xgui

import (
	//"fmt"
	//"github.com/davecgh/go-spew/spew"
	"bitbucket.org/coaljoe/lib/debug"
)

/*
// Convert pixels to units
func Px(v int) float64 {
	r := rx.Rxi().Renderer()
	//nativeResX := r.Width()
	//nativeResY := r.Height()
	return float64(v) * r.GetPw() // Fixme: use GetPixelSize?
}

// Convert units to pixels
func UnitsToPx(v float64) int {
	r := rx.Rxi().Renderer()
	return int(v * float64(r.Width())) // XXX: fixme?
}
*/

// Convert pixels to units (by width?)
// XXX use aspect to calculate PxH
func PxW(v int) float64 {
	//return float64(v)
	pw := 1.0 / float64(vars.resX)
	return (float64(v) * pw)
}

func PxH(v int) float64 {
	ph := 1.0 / float64(vars.resY)
	return (float64(v) * ph)
}

// Convert units to pixels
func UnitsToPxW(v float64) int {
	/*
		w, _, err := r.GetOutputSize()
		if err != nil {
			panic(err)
		}
	*/
	w := vars.resX
	return int(v * float64(w))
}

func UnitsToPxH(v float64) int {
	/*
		_, h, err := r.GetOutputSize()
		if err != nil {
			panic(err)
		}
	*/
	h := vars.resY
	return int(v * float64(h))
}

/*
func p_(args ...interface{}) {
	fmt.Println(args...)
}

func p(args ...interface{}) {
	fmt.Println(args...)
}
*/

var p = debug.P
var pp = debug.Pp
var pf = debug.Pf
var pv = debug.Pv
var pz = debug.Pz
var dump = debug.Dump
var pdump = debug.Pdump
var __xp = debug.Xp
var __xpp = debug.Xpp
var pq = debug.Pq

var __p = p
var _ = __p

var __pp = pp
var _ = __pp
