package xgui

type node struct {
	id   int
	name string
}

func newNode(name string) *node {
	return &node{id: _ider.GetNextId("node"),
		name: name}
}

func (n node) Name() string {
	return n.name
}

func (n node) Id() int {
	return n.id
}
