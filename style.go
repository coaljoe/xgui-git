package xgui

import (
	//"github.com/veandco/go-sdl2/sdl"

	. "bitbucket.org/coaljoe/xgui/types"
	//ttf "github.com/veandco/go-sdl2/ttf"
)

// XXX FIXME
var DebugFont FontI

// Style theme.
type Style struct {
	BgColor             Color //`name:bgColor`
	TextFont            FontI
	TextColor           Color
	textBgColor         Color
	borderColor         Color
	borderWidth         float64
	hoverBorderColor    Color
	hoverBorderWidth    float64
	OuterBorderWidth    int
	OuterBorderColor    Color
	PanelPatch9         *Patch9
	ButtonPatch9        *Patch9
	ButtonPressedPatch9 *Patch9
	SliderScoreBarColor Color
	Fonts               map[string]FontI
}

func NewStyle() *Style {
	// Default style

	//if font, err = ttf.OpenFont("res/gui/fonts/DejaVuSans.ttf", 16); err != nil {
	//font := LoadFont("res/gui/fonts/DejaVuSans.ttf", 18)
	//font := LoadFont("res/gui/fonts/FreeSans.ttf", 18)
	//font := LoadFont("res/gui/fonts/FreeMonoBold.ttf", 18)
	//font := LoadFont("res/gui/fonts/unifont.ttf", 20)
	//font := LoadFont("res/gui/fonts/LinLibertine_RI_G.ttf", 22)
	//font := LoadFont("res/gui/fonts/LinLibertine_R_G.ttf", 23)
	//font := LoadFont("res/gui/fonts/LinLibertine_Rah.ttf", 23)
	//font := LoadFont("res/gui/fonts/LinLibertine_Rah.ttf", 22)
	//font := LoadFont("res/gui/fonts/IBMPlexSans-Regular.ttf", 18)
	//font := LoadFont("res/gui/fonts/IBMPlexSans-Regular.ttf", 18)
	//font := LoadFont("res/gui/fonts/IBMPlexSans-Medium.ttf", 18)
	//font := LoadFont("res/gui/fonts/odin-rounded.light.ttf", 22)
	//font := LoadFont("res/gui/fonts/LinLibertine_RZI_G.ttf", 22)
	//font := LoadFont("res/gui/fonts/mplus-1c-regular.ttf", 18)
	font := NewFont()
	font.Load("res/gui/base/fonts/mplus-1mn-regular.ttf", 20)
	//DebugFont = LoadFont("res/gui/fonts/DejaVuSans.ttf", 16)
	DebugFont = NewFont()
	DebugFont.Load("res/gui/base/fonts/TerminusTTF.ttf", 12)

	s := &Style{
		BgColor:  Color{12, 12, 25, 25},
		TextFont: font,
		//textFont:         newFont(rx.ConfGetResPath()+"/gui/fonts/DejaVuSans.ttf", 16),
		//textColor:           Color{102, 102, 52, 255}, //ColorRed, //ColorBlack,
		//textColor:           Color{102, 102, 52, 255}, //ColorRed, //ColorBlack,
		//TextColor:           Color{50, 50, 50, 255}, //ColorRed, //ColorBlack,
		TextColor:        Color{50, 60, 70, 255}, //ColorRed, //ColorBlack,
		textBgColor:      ColorWhite,
		borderWidth:      PxW(2), // XXX use PxW and PxH?
		borderColor:      Color{25, 102, 102, 255},
		hoverBorderColor: Color{0, 0, 204},
		hoverBorderWidth: PxW(5),
		OuterBorderWidth: 0, // For debug
		//OuterBorderWidth:    2,
		OuterBorderColor: ColorBlack,
		//OuterBorderColor: ColorGreen,
		PanelPatch9:         NewPatch9(),
		ButtonPatch9:        NewPatch9(),
		ButtonPressedPatch9: NewPatch9(),
		//SliderScoreBarColor: Color{0, 128, 128, 255},
		//SliderScoreBarColor: Color{54, 117, 136, 255},
		SliderScoreBarColor: Color{122, 134, 146, 255},
		Fonts:               make(map[string]FontI),
	}

	// Add default fonts
	s.Fonts["text_font"] = s.TextFont
	s.Fonts["debug_font"] = DebugFont

	// Add extra fonts
	boldFont := NewFont()
	boldFont.Load("res/gui/base/fonts/mplus-1mn-bold.ttf", 20)
	s.Fonts["text_font_bold"] = boldFont
	boldFont2 := NewFont()
	boldFont2.Load("res/gui/base/fonts/mplus-1p-bold.ttf", 22)
	s.Fonts["text_font_bold2"] = boldFont2

	//pp(s.Fonts)

	/*
		s.panelPatch9.Load("tmp/gui_patch9/button9.9.png")
		s.panelPatch9.Rects = [9]Rect{}
		s.panelPatch9.Rects[0] = Rect{0, 0, 3, 10} // Top-left
		s.panelPatch9.Rects[1] = Rect{3, 0, 3, 10} // Top
		s.panelPatch9.Rects[2] = Rect{int32(s.panelPatch9.im.W() - 3), 0, 3, 10} // Top-right
		s.panelPatch9.Rects[3] = Rect{0, 3, 3, 10} // Left
		s.panelPatch9.Rects[5] = Rect{int32(s.panelPatch9.im.W() - 3), 3, 3, 10} // Right
		s.panelPatch9.Rects[6] = Rect{0, 25, 3, 3} // Bottom-left
		s.panelPatch9.Rects[7] = Rect{3, 25, 3, 3} // Bottom
		s.panelPatch9.Rects[8] = Rect{int32(s.panelPatch9.im.W() - 3), 25, 3, 3} // Bottom-right
	*/
	// Panel patch9
	//s.panelPatch9.Load("tmp/gui_patch9/panel.9.png")
	s.PanelPatch9.Load("res/gui/base/elements/panel.9.png")
	s.PanelPatch9.Rects = [9]Rect{}
	s.PanelPatch9.Rects[0] = Rect{0, 0, 3, 3}                         // Top-left
	s.PanelPatch9.Rects[1] = Rect{3, 0, 3, 3}                         // Top
	s.PanelPatch9.Rects[2] = Rect{s.PanelPatch9.Im.W() - 3, 0, 3, 3}  // Top-right
	s.PanelPatch9.Rects[3] = Rect{0, 3, 3, 3}                         // Left
	s.PanelPatch9.Rects[4] = Rect{3, 3, 16, 16}                       // Central
	s.PanelPatch9.Rects[5] = Rect{s.PanelPatch9.Im.W() - 3, 3, 3, 3}  // Right
	s.PanelPatch9.Rects[6] = Rect{0, 25, 3, 3}                        // Bottom-left
	s.PanelPatch9.Rects[7] = Rect{3, 25, 3, 3}                        // Bottom
	s.PanelPatch9.Rects[8] = Rect{s.PanelPatch9.Im.W() - 3, 25, 3, 3} // Bottom-right

	// Button patch9
	//s.buttonPatch9.Load("tmp/gui_patch9/button.9.png")
	s.ButtonPatch9.Load("res/gui/base/elements/button.9.png")
	s.ButtonPatch9.Rects = [9]Rect{}
	s.ButtonPatch9.Rects[0] = Rect{0, 0, 3, 3}                         // Top-left
	s.ButtonPatch9.Rects[1] = Rect{3, 0, 3, 3}                         // Top
	s.ButtonPatch9.Rects[2] = Rect{s.PanelPatch9.Im.W() - 3, 0, 3, 3}  // Top-right
	s.ButtonPatch9.Rects[3] = Rect{0, 3, 3, 3}                         // Left
	s.ButtonPatch9.Rects[4] = Rect{3, 3, 16, 16}                       // Central
	s.ButtonPatch9.Rects[5] = Rect{s.PanelPatch9.Im.W() - 3, 3, 3, 3}  // Right
	s.ButtonPatch9.Rects[6] = Rect{0, 25, 3, 3}                        // Bottom-left
	s.ButtonPatch9.Rects[7] = Rect{3, 25, 3, 3}                        // Bottom
	s.ButtonPatch9.Rects[8] = Rect{s.PanelPatch9.Im.W() - 3, 25, 3, 3} // Bottom-right

	// Button pressed patch9
	//s.buttonPressedPatch9.Load("tmp/gui_patch9/button_pressed.9.png")
	s.ButtonPressedPatch9.Load("res/gui/base/elements/button_pressed.9.png")
	s.ButtonPressedPatch9.Rects = [9]Rect{}
	s.ButtonPressedPatch9.Rects[0] = Rect{0, 0, 3, 3}                         // Top-left
	s.ButtonPressedPatch9.Rects[1] = Rect{3, 0, 3, 3}                         // Top
	s.ButtonPressedPatch9.Rects[2] = Rect{s.PanelPatch9.Im.W() - 3, 0, 3, 3}  // Top-right
	s.ButtonPressedPatch9.Rects[3] = Rect{0, 3, 3, 3}                         // Left
	s.ButtonPressedPatch9.Rects[4] = Rect{3, 3, 16, 16}                       // Central
	s.ButtonPressedPatch9.Rects[5] = Rect{s.PanelPatch9.Im.W() - 3, 3, 3, 3}  // Right
	s.ButtonPressedPatch9.Rects[6] = Rect{0, 25, 3, 3}                        // Bottom-left
	s.ButtonPressedPatch9.Rects[7] = Rect{3, 25, 3, 3}                        // Bottom
	s.ButtonPressedPatch9.Rects[8] = Rect{s.PanelPatch9.Im.W() - 3, 25, 3, 3} // Bottom-right

	//pp(s.panelPatch9.Rects)
	return s
}

func (s *Style) GetBaseStyle() *Style {
	return s
}

func (s *Style) SetBaseStyle(st *Style) {
	*s = *st
}

func (s *Style) CopyBaseStyle() *Style {
	styleCopy := *s
	return &styleCopy
}
