package xgui

import (
	. "bitbucket.org/coaljoe/xgui/types"
)

type SheetI interface {
	GetWidgets() []WidgetI
	Render()
	Update(dt float64)
}

// Base Sheet.
type Sheet struct {
	id      int
	name    string
	enabled bool
	root    *Widget
}

func (s *Sheet) Root() *Widget { return s.root }

func NewSheet(name string) *Sheet {
	s := &Sheet{
		name: name,
		root: NewWidget("root")}
	s.root.rect = Rect{0, 0, 0, 0}
	return s
}

// Get all sheets widgets.
func (s *Sheet) GetWidgets() []WidgetI {
	return s.root.GetAllChildren()
}

func (s *Sheet) GetWidgetByName(name string) WidgetI {
	ws := s.GetWidgets()
	for _, w := range ws {
		if w.Name() == name {
			return w
		}
	}
	pp("error: widget not found; name:", name)
	return nil
}

func (s *Sheet) GetWidgetById(id int) WidgetI {
	ws := s.GetWidgets()
	for _, w := range ws {
		if w.Id() == id {
			return w
		}
	}
	pp("error: widget not found; id:", id)
	return nil
}

// A shortcut for Root().AddChild()
//func (s *Sheet) AddWidget(w []WidgetI) {
//s.Root().AddChild(w)
//}

/*
// XXX cannot use it: will add base Sheet to system instead of
// actual object
func (s *Sheet) Show() {
	ctx.SheetSys.SetActiveSheet(SheetI(s))
}
*/

func (s *Sheet) Render() {
	//_log.Dbg("%F")
	//s.root.Render(r)
	for _, c := range s.root.Children() {
		p("render child")
		c.Render()
	}
}

var _ = `
func (s *Sheet) Render(r *rx.Renderer) {
	//s.root.Render(r)
	for _, c := range s.root.Children() {
		println("render child")
		c.Render(r)
	}
}
`

func (s *Sheet) Update(dt float64) {
	//_log.Dbg("%F")
	for _, c := range s.root.Children() {
		p("update child")
		c.Update(dt)
	}
}
