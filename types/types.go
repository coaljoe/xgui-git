package types

import (
	. "bitbucket.org/coaljoe/rx/math"
)

var (
	ColorWhite       = Color{255, 255, 255, 255}
	ColorBlack       = Color{0, 0, 0, 255}
	ColorRed         = Color{255, 0, 0, 255}
	ColorGreen       = Color{0, 255, 0, 255}
	ColorBlue        = Color{0, 0, 255, 255}
	ColorTransparent = Color{0, 0, 0, 0}
)

////////
// Color

type Color [4]int

func (c Color) R() int { return c[0] }
func (c Color) G() int { return c[1] }
func (c Color) B() int { return c[2] }
func (c Color) A() int { return c[3] }

func (c Color) ToVec4() Vec4 {
	return Vec4{float64(c[0]) / 255.0, float64(c[1]) / 255.0, float64(c[2]) / 255.0, float64(c[3]) / 255.0}
}

func (c Color) FromVec4(v Vec4) Color {
	return Color{int(v[0] * 255), int(v[1] * 255), int(v[2] * 255), int(v[3] * 255)}
}

///////
// Rect

//type Rect Vec4 // x, y, w, h
type Rect [4]int

func (r Rect) X() int       { return r[0] }
func (r Rect) Y() int       { return r[1] }
func (r Rect) W() int       { return r[2] }
func (r Rect) H() int       { return r[3] }
func (r Rect) X0() int      { return r[0] }
func (r Rect) Y0() int      { return r[1] }
func (r Rect) X1() int      { return r[0] + r[2] }
func (r Rect) Y1() int      { return r[1] + r[3] }
func (r Rect) ToVec4() Vec4 { return Vec4{float64(r[0]), float64(r[1]), float64(r[2]), float64(r[3])} }

func (r *Rect) SetX(v int) { r[0] = v }
func (r *Rect) SetY(v int) { r[1] = v }
func (r *Rect) SetW(v int) { r[2] = v }
func (r *Rect) SetH(v int) { r[3] = v }

//////
// Pos

//type Pos Vec2
type Pos [2]int
