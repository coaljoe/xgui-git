package xgui

import (
	//"github.com/veandco/go-sdl2/sdl"
	. "bitbucket.org/coaljoe/xgui/types"
	//ttf "github.com/veandco/go-sdl2/ttf"
	"bitbucket.org/coaljoe/xgui/backend"
)

type TextRenderer struct {
	defaultFont  FontI // Fixme: add changeable fonts?
	defaultColor Color
	btr          backend.TextRendererI
}

func newTextRenderer() *TextRenderer {
	tr := &TextRenderer{
		//defaultColor: ColorBlack,
		defaultColor: ColorRed,
	}
	tr.init()
	return tr
}

func (tr *TextRenderer) init() {
	tr.defaultFont = ctx.Style.TextFont
	tr.btr = ctx.Backend.TextRenderer()
}

// Render text with default color.
func (tr *TextRenderer) RenderText(text string, x, y int, font FontI) {
	if text == "" {
		return
	}
	xfont := font
	if xfont == nil {
		xfont = tr.defaultFont
	}
	tr.btr.RenderTextColor(text, x, y, tr.defaultColor, xfont)
}

func (tr *TextRenderer) RenderTextColor(text string, x, y int, color Color, font FontI) {
	if text == "" {
		return
	}
	tr.btr.RenderTextColor(text, x, y, color, font)
}

func (tr *TextRenderer) RenderTextColorAsImage(text string, x, y int, color Color, font FontI) ImageI {
	if text == "" {
		// fixme?
		panic("text can't be empty")
	}
	return tr.btr.RenderTextColorAsImage(text, x, y, color, font)
}

func (tr *TextRenderer) SizeText(text string, font FontI) (int, int) {
	if text == "" {
		return 0, 0
	}
	w, h := tr.btr.SizeText(text, font)
	return w, h
}

var _ = `
func (tr *TextRenderer) RenderTextColor(text string, x, y float64, color Color) {

	sx := 2.0 / float64(rxi.Renderer().Width())
	sy := 2.0 / float64(rxi.Renderer().Height())

	x = -1.0 + (x * 2)
	y = (y * 2) - 1.0

	//p(tr.font.ft_font.MaxAdvanceHeight)
	//pp(tr.font.ft_font.BBox)
	//yAdj := float64(tr.font.ft_font.MaxAdvanceHeight/64) * rxi.Renderer().GetPh()
	//yAdj := float64(tr.font.ft_font.Ascender/64) * sy
	yAdj := float64(tr.font.Height()*2) * rxi.Renderer().GetPh()
	//yAdj := float64(3442/64) * rxi.Renderer().GetPh()
	y += yAdj
	//pp(float64(tr.font.ft_font.LineHeight/64) * rxi.Renderer().GetPh())

	// FIXME: text rendering doesn't work with culling enabled
	// disable culling for whole GUI pass?
	prevCullEnabled := false
	if gl.IsEnabled(gl.CULL_FACE) {
		prevCullEnabled = true
		gl.Disable(gl.CULL_FACE)
	}

	prevBlendEnabled := false
	if gl.IsEnabled(gl.BLEND) {
		prevBlendEnabled = true
	} else {
		gl.Enable(gl.BLEND)
		gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	}
	gl.BindTexture(gl.TEXTURE_2D, tr.gl_tex)

	tr.shader.Bind()
	tr.shader.PassUniform1i("tex", 0)
	tr.shader.PassUniform4f_("color", color.ToVec4())

	gl.EnableVertexAttribArray(uint32(tr.gl_attribute_coord))
	gl.BindBuffer(gl.ARRAY_BUFFER, tr.gl_vbo)
	gl.VertexAttribPointer(uint32(tr.gl_attribute_coord), 4, gl.FLOAT,
		false, 0, gl.PtrOffset(0))

	for runeIdx, rune := range text {
		//c := text[i]
		//fmt.Printf("rune: %#U\n", rune)

		skipRendering := false
		if unicode.IsSpace(rune) {
			skipRendering = true
		}

		font := tr.font.ft_font
		gIndex := font.Index(rune)
		//p("index:", gIndex)
		g, err := font.Load(gIndex)
		if err != nil {
			panic(fmt.Sprintf(
				"Failed to load glyph: %#U at byte position %d",
				rune, runeIdx))
		}
		gImage, err := g.Image()
		if err != nil {
			panic(err)
		}
		_ = gImage

		// Size in pixels
		gWidth := g.Width / 64
		gHeight := g.Height / 64

		//assert(gImage.ColorModel() == _color.AlphaModel)
		//p(len(gImage.Pix), g.Width, g.Height, gImage.Bounds())
		//pp(2)

		if false && !unicode.IsSpace(rune) {
			outFile, err := os.Create("/tmp/rxgui_test_freetype_out.png")
			if err != nil {
				panic(err)
			}

			err = png.Encode(outFile, gImage)
			if err != nil {
				panic(err)
			}
			println("wrote /tmp/rxgui_test_freetype_out.png file")
		}

		if !skipRendering {
			// Upload the glyph's image
			gl.TexImage2D(gl.TEXTURE_2D, 0, gl.ALPHA,
				int32(gWidth), int32(gHeight),
				0, gl.ALPHA, gl.UNSIGNED_BYTE, gl.Ptr(gImage.Pix))
		}

		// Calculate vertex and texture coord
		bitmap_left := float32(g.HMetrics.BearingX / 64)
		//bitmap_top := float32(-g.VMetrics.BearingY / 64)
		bitmap_top := float32(g.VMetrics.BearingY / 64)
		bitmap_width := float32(gWidth)
		bitmap_rows := float32(gHeight)

		x2 := float32(x) + bitmap_left*float32(sx)
		y2 := float32(-y) + bitmap_top*float32(sy)
		w := bitmap_width * float32(sx)
		h := bitmap_rows * float32(sy)

		// x, y, s, t
		box := []float32{
			x2, -y2, 0, 0,
			x2 + w, -y2, 1, 0,
			x2, -y2 - h, 0, 1,
			x2 + w, -y2 - h, 1, 1,
		}
		//p(box)

		// Draw the character on the screen
		gl.BufferData(gl.ARRAY_BUFFER, 16*4, gl.Ptr(&box[0]), gl.DYNAMIC_DRAW)
		//gl.BufferData(gl.ARRAY_BUFFER, 16*4, gl.Ptr(box), gl.DYNAMIC_DRAW)
		gl.DrawArrays(gl.TRIANGLE_STRIP, 0, 4)

		// Advance the cursor to the start of the next character
		advance_x := g.HMetrics.Advance / 64
		//pp(advance_x, float64(advance_x)*sx)
		//advance_y := g.VMetrics.Advance / 64
		//p(advance_y, float64(advance_y)*sy)
		//x += float64(advance_x>>6) * sx
		//y += float64(advance_y>>6) * sy
		//x += 0.05
		x += float64(advance_x) * sx
		//y += float64(advance_y) * sy
		y += 0
	}

	gl.DisableVertexAttribArray(uint32(tr.gl_attribute_coord))
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindTexture(gl.TEXTURE_2D, 0)
	tr.shader.Unbind()

	if !prevBlendEnabled {
		gl.Disable(gl.BLEND)
	}

	if prevCullEnabled {
		gl.Enable(gl.CULL_FACE) // Restore culling
	}

	rx.Xglcheck()
}
`
