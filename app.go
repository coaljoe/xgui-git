package xgui

var _ = `

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"os"
	"time"
)

var renderer *sdl.Renderer

// BaseApp
type App struct {
	width      int
	height     int
	fullscreen bool
	title      string
	window     *sdl.Window
	running    bool
	frame      int
	lastTime   time.Time
	dt         float64
}

func (a *App) Renderer() *sdl.Renderer {
	return renderer
}

func (a *App) Dt() float64 {
	return a.dt
}

func NewApp() *App {
	a := &App{
		title: "xgui",
	}
	return a
}

func (a *App) Init(width, height int, fullscreen bool) {
	// check args
	if width < 1 || height < 1 {
		panic("bad width and height")
	}

	a.width = width
	a.height = height
	a.fullscreen = fullscreen

	ctx.Backend.InitWindow(width, height, fullscreen)
	renderer = ctx.Backend.Renderer()

	var err error
	if !fullscreen {
		a.window, err = sdl.CreateWindow(a.title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
			int32(width), int32(height), sdl.WINDOW_SHOWN|sdl.WINDOW_OPENGL)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
			panic(1)
		}
	} else {
		flags := uint32(sdl.WINDOW_SHOWN | sdl.WINDOW_OPENGL)
		flags |= sdl.WINDOW_FULLSCREEN
		a.window, err = sdl.CreateWindow(a.title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
			int32(vars.resX), int32(vars.resY), flags
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
			panic(1)
		}
	}

	renderer, err = sdl.CreateRenderer(a.window, -1, sdl.RENDERER_ACCELERATED|sdl.RENDERER_PRESENTVSYNC)
	//renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		panic(2)
	}
	//defer renderer.Destroy()

	// XXX set the default to 0 explicitly (seems not presented on all sdl versions)
	sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "0")

	a.running = true
}

func (a *App) Draw() {
	// Reset Color
	renderer.SetDrawColor(0, 0, 0, 255)
	// Clear
	renderer.Clear()

	// Draw gui here
	xgi.Render(renderer)

	renderer.Present()
}

func (a *App) Step() bool {
	if !a.running {
		a.Quit()
		return false
	}

	thisTime := time.Now()
	passedTime := thisTime.Sub(a.lastTime)
	a.dt = passedTime.Seconds()

	ctx.Backend.ProcessEvents()


	a.Update(a.dt)
	a.Draw()

	a.lastTime = thisTime
	a.frame += 1

	return true
}

func (a *App) Quit() {
	println("App.Quit")
	a.running = false
	sdl.Quit()
}

func (a *App) Update(dt float64) {

}
`
