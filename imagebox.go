package xgui

import (
	. "bitbucket.org/coaljoe/xgui/types"
	//"github.com/veandco/go-sdl2/sdl"
)

type ImageBoxStyle struct {
	*Style
}

func NewImageBoxStyle() *ImageBoxStyle {
	styleCopy := *ctx.Style
	s := &ImageBoxStyle{
		Style: &styleCopy,
	}
	return s
}

// A widget.
type ImageBox struct {
	*Widget
	Style      *ImageBoxStyle
	Image      ImageI
	ScaleImage bool
}

func NewImageBox(rect Rect) *ImageBox {
	ib := &ImageBox{
		Widget:     NewWidget("ImageBox"),
		ScaleImage: false,
	}
	ib.Style = NewImageBoxStyle()
	ib.BaseStyle = ib.Style.Style
	ib.rect = rect
	return ib
}

func (ib *ImageBox) SetImage(path string) {
	if ib.Image == nil {
		ib.Image = ctx.Backend.NewImage()
	}
	ib.Image.SetScaleFilter(true)
	ib.Image.Create(5, 5)
	ib.Image.Load(path)
}

func (ib *ImageBox) Render() {
	r := ctx.Renderer
	_ = r

	if !ib.visible {
		return
	}

	ib.Widget.Render()
	defer ib.Widget.RenderChildren()

	if ib.Image != nil {
		absRect := ib.GetAbsoluteRect()
		x := absRect.X()
		y := absRect.Y()
		w := ib.Image.W()
		h := ib.Image.H()

		if ib.ScaleImage {
			w = ib.rect.W()
			h = ib.rect.H()
		}

		ib.Image.Render(x, y, w, h)
	}
}

func (ib *ImageBox) Update(dt float64) {
	ib.Widget.Update(dt)
}
