package xgui

var conf = NewDefaultConf()

func GetConf() *Conf { return conf }

type Conf struct {
	DebugOutput  bool
	VerboseLevel int // XXX TODO
}

// New default conf
func NewDefaultConf() *Conf {
	c := &Conf{
		DebugOutput:  false,
		VerboseLevel: 0,
	}
	return c
}
