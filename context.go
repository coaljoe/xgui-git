package xgui

import (
	"bitbucket.org/coaljoe/xgui/backend"
	//"github.com/veandco/go-sdl2/sdl"
)

// Gui Context.
type Context struct {
	//Backend BackendI
	Backend backend.BackendI
	//Renderer *sdl.Renderer
	Renderer backend.RendererI // shortcut for Backend.Renderer()
	SheetSys *SheetSys
	InputSys *InputSys
	Style    *Style
	//Font         *ft.Font
	TextRenderer *TextRenderer
	//Renderables RenderableI
}

func newContext() *Context {
	return &Context{}
}
