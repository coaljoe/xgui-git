package xgui

const ()

var ()

var vars struct {
	// Screen resolution
	resX int
	resY int
	// Native screen resolution for fixed-size elements
	nativeResX int
	nativeResY int
	// Resolution scale coeff. for fixed-size elements
	resScaleX float64
	resScaleY float64
	// Time Delta
	dt float64
	// Mouse position
	mx int
	my int
	// Dev mode
	dev bool
}

func SetDefaultVars() {
	//vars.resX = 1024
	//vars.resY = 576
	vars.nativeResX = 1024
	vars.nativeResY = 576
	vars.resScaleX = 1
	vars.resScaleY = 1
	vars.dt = 0
	vars.mx = -1
	vars.my = -1
}
